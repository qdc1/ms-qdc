/* eslint-disable no-param-reassign */
const { Model } = require('sequelize');

/**
 * @typedef {import('sequelize').Sequelize} Sequelize
 * @typedef {import('sequelize/types')} DataTypes
 */

/**
 * @param {DataTypes} DataTypes
 * @param {Sequelize} sequelize
 */

module.exports = (sequelize, DataTypes) => {
  class Gauge extends Model {
    /**
     * Helper method for defining associations.
     * This method is not a part of Sequelize lifecycle.
     * The `models/index` file will call this method automatically.
     */
    static associate() {
      // define association here
    }
  }
  Gauge.init(
    {
      DieId: { field: "DIE_ID", type: DataTypes.INTEGER ,primaryKey: true },
      DiesTypeId: { field: "DIES_TYPE_ID", type: DataTypes.INTEGER  },
      DieName : { field: "DIE_NM", type: DataTypes.STRING  },
      DieDesc: { field: "DIE_DESC", type: DataTypes.STRING  ,allowNull: true  },
      DieAssetNo: { field: "DIE_ASSET_NO", type: DataTypes.STRING  ,allowNull: true  },
      DieVendor: { field: "DIE_VENDOR", type: DataTypes.STRING  ,allowNull: true  },
      DailyDieShot: { field: "DAILY_DIE_SHOT", type: DataTypes.INTEGER  },
      RetrieveFlag: { field: "RETRIEVE_FLAG", type: DataTypes.STRING  },
      TotalShoot: { field: "TOTAL_SHOOT", type: DataTypes.INTEGER  },
      DieDeleteStatus: { field: "DIE_DEL_STS", type: DataTypes.STRING  },
      CreateDate: { field: "CREATED_DT", type: DataTypes.DATE  },
      CreateBy: { field: "CREATED_BY", type: DataTypes.STRING  },
      ChangedDate: { field: "CHANGED_DT", type: DataTypes.DATE  ,allowNull: true  },
      ChangedBy: { field: "CHANGED_BY", type: DataTypes.STRING  ,allowNull: true  },
    },
    {
      sequelize,
      modelName: 'Gauge',
      tableName: 'TB_M_QDC_GAUGE',
      timestamps: false,
      paranoid: true,
    }
  );
  Gauge.addHook('beforeCreate', async (Gauge) => {
    // define addHook here
  });
  return Gauge;
};

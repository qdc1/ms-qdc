module.exports.GaugeController = require('./qdc/Gauge.controller');
module.exports.GaugeDetailController = require('./qdc/GaugeDetail.controller');
module.exports.QulaityOperationalController = require('./qdc/QulaityOperational.controller');
module.exports.CalibrationController = require('./qdc/Calibration.controller');
module.exports.CalibrationItemController = require('./qdc/CalibrationItem.controller');
module.exports.MonthlyPlanningController = require('./qdc/MonthlyPlanning.controller');
module.exports.MonthlyPlanningHolidayController = require('./qdc/MonthlyPlanningHoliday.controller');
module.exports.DailyManagementController = require('./qdc/DailyManagement.controller');
module.exports.CustomPlanningController = require('./qdc/CustomPlanning.controller');
module.exports.ReportPlanningController = require('./qdc/ReportPlanning.controller');
module.exports.MessageController = require('./qdc/Message.controller');
module.exports.CommonController = require('./qdc/Common.controller');
/*define other in here*/
const httpStatus = require('http-status');
const readXlsxFile = require("read-excel-file/node");
const excel = require("exceljs");
const pick = require('../../utils/pick');
const ApiError = require('../../utils/ApiError');
const catchAsync = require('../../utils/catchAsync');
const { GaugeService } = require('../../services');
const CommonAPI = require('../../utils/CommonAPI');
const moment = require('moment');

const createGauge = catchAsync(async (req, res) => {
  let gaugeDetail = req.body.gauge_detail
  if(gaugeDetail && gaugeDetail.length > 0){
    req.body.gauge_detail = JSON.stringify(gaugeDetail)
  }else{
    req.body.gauge_detail = '[]'
  }

  const Gauge = await GaugeService.createGauge(req.body,req.app.locals.userId);
  let response = Gauge[0][0].response
  if (response == 200){
    res.send({
      status: httpStatus.OK,
      message: "Successfully inserted data Gauge"
    });
  }else{
    res.send({
      status: httpStatus.FORBIDDEN,
      message: "Failed to insert data Gauge [Data Duplicate]"
    });
  }
});

const searchGauge = catchAsync(async (req, res) => {
  const filter = req.body.search;
  const options = pick(req.body, ['sortBy','sortOrder', 'limit', 'page']);
  const result = await GaugeService.queryGauges(filter, options,req.app.locals.userId);
  res.send({
    status:httpStatus.OK,
    message:null,
    details:null,
    data:result.rows,
    totalRecords:result.count,
    totalPages:Math.ceil(result.count/req.body.limit),
    pageNo:req.body.page,
    pageSize:req.body.limit
  });
});

const deleteGauge = catchAsync(async (req, res) => {
  const Gauge = await GaugeService.getGauge(req.body,req.app.locals.userId);
  if (Gauge[0].length == 0) {
    res.send({
      status:httpStatus.NOT_FOUND,
      message:"Gauge not found",
    });
  }else{
    await GaugeService.deleteGauge(req.body,req.app.locals.userId);
    res.send({
      status:httpStatus.OK,
      message:"Gauge successfuly deleted",
    });
  }
});

const getGauge = catchAsync(async (req, res) => {
  const Gauge = await GaugeService.getGauge(req.body,req.app.locals.userId);
  if (Gauge[0].length == 0) {
    res.send({
      status:httpStatus.OK,
      message: 'Gauge not found',
      data: null,
      details: null
    });
  }else{
    res.send({
      status:httpStatus.OK,
      message: null,
      data: Gauge[0],
      details: null
    });
  }
});

const updateGauge = catchAsync(async (req, res) => {
  let gaugeDetail = req.body.gauge_detail
  if(gaugeDetail && gaugeDetail.length > 0){
    req.body.gauge_detail = JSON.stringify(gaugeDetail)
  }else{
    req.body.gauge_detail = '[]'
  }

  const Gauge = await GaugeService.getGauge(req.body,req.app.locals.userId);
  if (Gauge[0].length == 0) {
    res.send({
      status:httpStatus.NOT_FOUND,
      message:"Gauge not found",
    });
  }else{
    await GaugeService.updateGauge(req.body,req.app.locals.userId);
    res.send({
      status:httpStatus.OK,
      message:"Gauge successfuly updated",
    });
  }
});

const uploadGauge = catchAsync(async (req, res) => {
  try {
    const workbook = new excel.Workbook();
    await workbook.xlsx.load(Buffer.from(req.body.file, 'base64')).then(async () => {
          let ws = workbook.getWorksheet('GAUGE');
          let existRow = true;
          let currRow = 1;
          let dataRows = [];
          if (ws.getCell('G' + currRow).value == 'PRODUCT_RANGE_MEASUREMENT'){
            while (existRow) {
              currRow++;
              let data = {
                GAUGE_SERIAL_NO: ws.getCell('B' + currRow).value,
                GAUGE_NAME: ws.getCell('C' + currRow).value,
                GAUGE_BRAND: ws.getCell('D' + currRow).value,
                GAUGE_LINE_NAME: ws.getCell('E' + currRow).value,
                GAUGE_OP_NO: ws.getCell('F' + currRow).value,
                PRODUCT_RANGE_MEASUREMENT: ws.getCell('G' + currRow).value,
                CALIBRAATION_EQUIPMENT: ws.getCell('H' + currRow).value,
                GAUGE_VALID_FOR: ws.getCell('I' + currRow).value,
                GAUGE_INSTALL_CALIBRATION: moment(ws.getCell('J' + currRow).value, 'DD/MM/YYYY').format('YYYY-MM-DD'),
                GAUGE_LAST_CALIBRATION: ws.getCell('K' + currRow).value != '' && ws.getCell('K' + currRow).value != null ? moment(ws.getCell('K' + currRow).value, 'DD/MM/YYYY').format('YYYY-MM-DD') : null,
                GAUGE_LAST_JUDGEMENT: ws.getCell('L' + currRow).value,
                GAUGE_PIC: ws.getCell('M' + currRow).value,
                STATUS: 1,
                CREATED_BY: req.app.locals.userId,
                CREATED_DT: new Date(),
                CHANGED_BY: req.app.locals.userId,
                CHANGED_DT: new Date()
              }
              if(data.GAUGE_SERIAL_NO == null && data.GAUGE_NAME == null){
                existRow = false;
              }else{
                dataRows.push(data)
              }
            }
            let obj = {jsonData: JSON.stringify(dataRows)}
            console.log('DATE UPLOAD', new Date(), '====>', obj)
            const Gauge = await GaugeService.uploadGauge(obj,req.app.locals.userId);
            let response = Gauge[0][0].response
            if (response == 200){
              res.send({
                status: httpStatus.OK,
                message: "Successfully upload data Gauge"
              });
            }else{
              res.send({
                status: httpStatus.FORBIDDEN,
                message: "Failed to upload data Gauge"
              });
            }
          }else{
            console.log('tetet')
            res.send({
              status: httpStatus.FORBIDDEN,
              message: "Please, update the form template"
            });
          }
    })
  } catch (e) {
      res.send({
        status: httpStatus.INTERNAL_SERVER_ERROR,
        message: "Failed Upload Master Gauge"
      });
  }
});

const templateGauge = catchAsync(async (req, res) => {
  try {
    let filePath = __basedir + "/src/template/TEMPLATE_MASTER_GAUGE.xlsx"
    
    const workbook = new excel.Workbook();
    await workbook.xlsx.readFile(filePath).then(async () => {
        console.log('SUCCESS DOWNLOAD TEMPLATE GAUGE')
      }).catch(err => {
        console.log(err.message);
      });

    res.setHeader(
      "Content-Type",
      "application/vnd.openxmlformats-officedocument.spreadsheetml.sheet"
    );
    res.setHeader(
      "Content-Disposition",
      "attachment; filename=" + `TEMPLATE_UPLOAD_GAUGE [${moment(new Date()).format('DD MMM YYYY HHmmss')}].xlsx`
    );
    await workbook.xlsx.write(res)
      .then(async function (data) {
        res.end();
        console.log('File write done........');
      });
  } catch (e) {
    console.log('ERROR CATCH', e)
  }
});

const downloadGauge = catchAsync(async (req, res) => {
  try {

    let filePath = __basedir + "/src/template/REPORT_MASTER_GAUGE.xlsx"

    const filter = {
      keyword: '',
      gaugeStatus: '',
      idGauges: '',
      lineName: ''
    };
    const options = {
      page: 1,
      limit: 999999,
      sortBy: 'gauge_name',
      sortOrder: 'asc'
    };

    const result = await GaugeService.queryGauges(filter, options,req.app.locals.userId);
    let gauges = result.rows

    const workbook = new excel.Workbook();
    await workbook.xlsx.readFile(filePath).then(async () => {

      let ws = workbook.getWorksheet('GAUGE');
      if (gauges.length > 0) {
        let i = 0;
        for (i = 0; i < gauges.length; i++) {
          ws.getCell('A' + (i+2)).value = i+1;
          ws.getCell('B' + (i+2)).value = gauges[i].GAUGE_SERIAL_NO;
          ws.getCell('C' + (i+2)).value = gauges[i].GAUGE_NAME;
          ws.getCell('D' + (i+2)).value = gauges[i].GAUGE_BRAND;
          ws.getCell('E' + (i+2)).value = gauges[i].GAUGE_LINE_NAME;
          ws.getCell('F' + (i+2)).value = gauges[i].GAUGE_OP_NO;
          ws.getCell('G' + (i+2)).value = gauges[i].GAUGE_PRODUCT_RANGE_MEASUREMENT;
          ws.getCell('H' + (i+2)).value = gauges[i].GAUGE_CALIBRATION_EQUIPMENT;
          ws.getCell('I' + (i+2)).value = gauges[i].GAUGE_VALID_FOR;
          ws.getCell('J' + (i+2)).value = gauges[i].GAUGE_INSTALL_CALIBRATION;
          ws.getCell('K' + (i+2)).value = gauges[i].GAUGE_LAST_CALIBRATION;
          ws.getCell('L' + (i+2)).value = gauges[i].GAUGE_LAST_JUDGEMENT;
          ws.getCell('M' + (i+2)).value = gauges[i].GAUGE_PIC;
          ws.getCell('N' + (i+2)).value = gauges[i].GAUGE_NEXT_CALIBRATION;
          ws.getCell('O' + (i+2)).value = gauges[i].GAUGE_STATUS_CALIBRATION;
        }
      }

    }).catch(err => {
      console.log(err.message);
    });

    res.setHeader(
      "Content-Type",
      "application/vnd.openxmlformats-officedocument.spreadsheetml.sheet"
    );
    res.setHeader(
      "Content-Disposition",
      "attachment; filename=" + `MASTER_GAUGE [${moment(new Date()).format('DD MMM YYYY HHmmss')}].xlsx`
    );
    await workbook.xlsx.write(res)
      .then(async function (data) {
        res.end();
        console.log('File write done........');
      });

  } catch (e) {
    console.log('ERROR CATCH', e)
  }
});


const getGaugeProblem = catchAsync(async (req, res) => {
  const filter = req.body.search;
  const options = pick(req.body, ['sortBy','sortOrder', 'limit', 'page']);
  const result = await GaugeService.queryGaugeProblem(filter, options,req.app.locals.userId);
  res.send({
    status:httpStatus.OK,
    message:null,
    details:null,
    data:result.rows,
    totalRecords:result.count,
    totalPages:Math.ceil(result.count/req.body.limit),
    pageNo:req.body.page,
    pageSize:req.body.limit
  });
});

module.exports = {
  searchGauge,
  getGauge,
  deleteGauge,
  createGauge,
  updateGauge,
  uploadGauge,
  downloadGauge,
  templateGauge,
  getGaugeProblem
};

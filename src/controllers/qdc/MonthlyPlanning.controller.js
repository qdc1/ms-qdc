const httpStatus = require('http-status');
const readXlsxFile = require("read-excel-file/node");
const excel = require("exceljs");
const pick = require('../../utils/pick');
const ApiError = require('../../utils/ApiError');
const catchAsync = require('../../utils/catchAsync');
const { MonthlyPlanningService, MonthlyPlanningHolidayService } = require('../../services');
const moment = require('moment');

const searchMonthlyPlanning = catchAsync(async (req, res) => {
  if (req.body.isDetail == 'NO'){
    req.body.date = ''
  }
  const MonthlyPlanning = await MonthlyPlanningService.searchMonthlyPlanning(req.body, req.app.locals.userId);
  if (MonthlyPlanning[0].length == 0) {
    res.send({
      status:httpStatus.OK,
      message: 'Monthly Planning not found',
      data: null,
      details: null
    });
  }else{
    res.send({
      status:httpStatus.OK,
      message: null,
      data: MonthlyPlanning[0],
      details: null
    });
  }
});

const createMonthlyPlanning = catchAsync(async (req, res) => {

  try {
    let param = req.body
    let date = param.year+'-'+param.month
    
    //Get Data Date in month (Ambil semua tanggal di bulan sekarang)
    let allDateMonth = Array.from({length: moment(date).daysInMonth()}, (x, i) => moment(date).startOf('month').add(i, 'days').format('ddd-DD-MM-YYYY'))

    //Filter future Date (filter tanggal sekarang sampai dengan akhir bulan)
    let allDateMonthFuture = []
    allDateMonth.forEach(e => {
      let today = moment(new Date())
      let datePlanning = moment(e, 'ddd-DD-MM-YYYY').format('YYYY-MM-DD')
      let isFuture = today.diff(datePlanning, 'days');
      if (isFuture <= 0){
        allDateMonthFuture.push(e)
      }
    });

    //Filter weekends date (filter weekdays dibulan sekarang)
    let exWeekends = []
    allDateMonthFuture.forEach(e => {
      let split = e.split("-")
      // // Selain sabtu minggu // //
      // // ===================// //
      // if(split[0] != 'Sat' && split[0] != 'Sun'){
      //   let format = moment(split[1]+'-'+split[2]+'-'+split[3], 'DD-MM-YYYY').format('YYYY-MM-DD')
      //   exWeekends.push(format)
      // }
      // // Hanya selasa khamis // //
      // // ===================// //
      if(split[0] == 'Tue' || split[0] == 'Thu'){
        let format = moment(split[1]+'-'+split[2]+'-'+split[3], 'DD-MM-YYYY').format('YYYY-MM-DD')
        exWeekends.push(format)
      }
    });

    //Get Data Custom Date (Mengambil custom date, untuk pengecualian hari libur custom)
    let search = {
        search: { keyword: date },
        page: 1,
        limit: 100000,
        sortBy: 'gph_date',
        sortOrder: 'ASC'
    }
    const filter = search.search;
    const options = pick(search, ['sortBy','sortOrder', 'limit', 'page']);
    const result = await MonthlyPlanningHolidayService.searchMonthlyPlanningHoliday(filter, options,req.app.locals.userId);
    let dataCustomDate = result.rows

    //Filter data overtime and holiday (memisahkan tanggal weekdays dan weekend pada custom date)
    let holiday = []
    let overtime = []
    dataCustomDate.forEach(e => {
      if(e.GPH_IS_WORK == 'Y'){
        overtime.push(e.GPH_DATE)
      }else if (e.GPH_IS_WORK == 'N'){
        holiday.push(e.GPH_DATE)
      }
    });

    //Filter duplicate data overtime and holiday
    let uniqueHoliday = [...new Set(holiday)]
    let uniqueOvertime = [...new Set(overtime)]


    //Remove holiday date
    let finalDate = exWeekends.filter((value) => {
        return !uniqueHoliday.includes(value)
    })
    //Add overtime date
    finalDate = [...finalDate, ...uniqueOvertime]

    //Final check unique date
    finalDate = [...new Set(finalDate)]

    //Order date
    finalDate.sort(function(a,b){
      return new Date(a) - new Date(b);
    })

    //Filter future Date after final date (tanggal final yang akan dipakai buat generate)
    let finlFutureDate = []
    finalDate.forEach(e => {
      let today = moment(new Date())
      let datePlanning = moment(e, 'YYYY-MM-DD').format('YYYY-MM-DD')
      let isFuture = today.diff(datePlanning, 'days');
      if (isFuture <= 0){
        finlFutureDate.push(e)
      }
    });

    let objBetween = {
      startDate : moment(date, 'YYYY-MM').format('YYYY-MM-DD'),
      endDate : moment(date, 'YYYY-MM').add(1, 'M').format('YYYY-MM-DD')
    }

    //Get data gauge expired in last day month or future month
    let GaugeExp = null
    if (moment(objBetween.startDate).isSame(new Date(), 'month')){
      GaugeExp = await MonthlyPlanningService.getGaugeExpLastDay(objBetween,req.app.locals.userId)
    }else{
      GaugeExp = await MonthlyPlanningService.getGaugeExpMonth(objBetween,req.app.locals.userId)
    }
    let obj = {
      GpDate: date,
      GpDescription: 'Monthly Planning '+ date,
      DataGauges: []
    }

    // console.log(GaugeExp)
    let difficultGauge = []
    let easyGauge = []
    
    GaugeExp[0].forEach(e => {
      console.log(e)
      if(e.GAUGE_LEAD_TIME >= 3000){
        difficultGauge.push(e)
      }else{
        easyGauge.push(e)
      }
    });

    // // Function to distribute gauges evenly based on GAUGE_LEAD_TIME
    // function distributeGauges(gauges, dates) {
    //   // Sort gauges by GAUGE_LEAD_TIME in descending order
    //   gauges.sort((a, b) => b.GAUGE_LEAD_TIME - a.GAUGE_LEAD_TIME);
    //   // Initialize an array to hold the distributed gauges for each date
    //   let distributedGauges = dates.map(date => ({ date, gauges: [], totalLeadTime: 0 }));
    //   // Distribute gauges to dates in a balanced way
    //   for (let gauge of gauges) {
    //     // Find the date with the least total lead time
    //     distributedGauges.sort((a, b) => a.totalLeadTime - b.totalLeadTime);
    //     // Assign the gauge to this date
    //     distributedGauges[0].gauges.push(gauge);
    //     distributedGauges[0].totalLeadTime += gauge.GAUGE_LEAD_TIME;
    //   }
    //   return distributedGauges;
    // }

    function distributeGauges(gauges, dates) {
      // Initialize an array to hold the distributed gauges for each date
      let distributedGauges = dates.map(date => ({ date, gauges: [], totalLeadTime: 0 }));
    
      // Distribute gauges to the closest date based on GAUGE_NEXT_CALIBRATION
      for (let gauge of gauges) {
        // Find the closest date for this gauge
        let closestDate = dates.reduce((closest, currentDate) => {
          const currentDiff = Math.abs(new Date(currentDate) - new Date(gauge.GAUGE_NEXT_CALIBRATION));
          const closestDiff = Math.abs(new Date(closest) - new Date(gauge.GAUGE_NEXT_CALIBRATION));
          return currentDiff < closestDiff ? currentDate : closest;
        });
    
        // Find the corresponding date object in distributedGauges
        let targetDate = distributedGauges.find(d => d.date === closestDate);
    
        // Assign the gauge to this closest date and update the totalLeadTime
        targetDate.gauges.push(gauge);
        targetDate.totalLeadTime += gauge.GAUGE_LEAD_TIME;
      }
    
      return distributedGauges;
    }

    // Function to distribute gauges evenly based on GAUGE_LEAD_TIME
    function distributeGaugesShift(gauges, shifts) {
      // Sort gauges by GAUGE_LEAD_TIME in descending order
      gauges.sort((a, b) => b.GAUGE_LEAD_TIME - a.GAUGE_LEAD_TIME);
      // Initialize an array to hold the distributed gauges for each date
      let distributedGaugesShift = shifts.map(shift => ({ shift, gauges: [], totalLeadTime: 0 }));
      // Distribute gauges to dates in a balanced way
      for (let gauge of gauges) {
        // Find the date with the least total lead time
        distributedGaugesShift.sort((a, b) => a.totalLeadTime - b.totalLeadTime);
        // Assign the gauge to this date
        distributedGaugesShift[0].gauges.push(gauge);
        distributedGaugesShift[0].totalLeadTime += gauge.GAUGE_LEAD_TIME;
      }
      return distributedGaugesShift;
    }

    // Merge easy and diffulty gauge by date
    function mergeGauges(arr1, arr2) {
      // Create a map to store merged data
      const mergedMap = new Map();
      // Helper function to add data to the map
      function addToMap(arr) {
        arr.forEach(item => {
          if (mergedMap.has(item.date)) {
            const existingItem = mergedMap.get(item.date);
            existingItem.gauges = existingItem.gauges.concat(item.gauges);
            existingItem.totalLeadTime += item.totalLeadTime;
          } else {
            mergedMap.set(item.date, { ...item });
          }
        });
      }
      // Add both arrays to the map
      addToMap(arr1);
      addToMap(arr2);
      // Convert the map back to an array
      return Array.from(mergedMap.values());
    }

    // mendistribusikan gauge ke tanggal
    let easyGaugeDistribution = [] 
    let difficultGaugeDistribution = [] 
    let gaugeDistribution = []
    if(finlFutureDate.length > 0){
      // console.log(easyGauge)
      easyGaugeDistribution = distributeGauges(easyGauge, finlFutureDate);
      difficultGaugeDistribution = distributeGauges(difficultGauge, finlFutureDate);
    }
    let shifts = [1,2]

    // menggabungkan easy dan diffulty gauge
    gaugeDistribution = mergeGauges(easyGaugeDistribution, difficultGaugeDistribution);

    // mendistribusikan gauge per tanggal ke masing2 shift
    gaugeDistribution.forEach(e => {
      let gaugeDistributionShift = distributeGaugesShift(e.gauges, shifts);
      e.shift_distibution = gaugeDistributionShift
    });
    
    let DataGauges = []
    gaugeDistribution.forEach(e => {
      e.shift_distibution.forEach(e1 => {
        e1.gauges.forEach(e2 => {
          let data = { 
            GpdGaugeId:  e2.GAUGE_ID, 
            GpdDate: e.date,
            GpdShift: e1.shift
          }
          DataGauges.push(data)
        });
      })
    });

    obj.DataGauges = DataGauges

    let detail = obj.DataGauges
    if(detail && detail.length > 0){
      obj.DataGauges = JSON.stringify(detail)
    }else{
      obj.DataGauges = '[]'
    }

    const Generate = await MonthlyPlanningService.createMonthlyPlanning(obj,req.app.locals.userId);
    let response = Generate[0][0].response
    if (response == 200){
      res.send({
        status: httpStatus.OK,
        message: "Successfully generate monthly planning"
      });
    }

    // res.send({
    //   status: httpStatus.OK,
    //   message: "Successfully generate monthly planning"
    // });

  } catch (error) {
    console.log('ERROR GENERATE', error)
    res.send({
      status: httpStatus.INTERNAL_SERVER_ERROR,
      message: "Internal Server Error"
    });
  }

  
});

const createMonthlyPlanningBUFIX88 = catchAsync(async (req, res) => {

  try {
    let param = req.body
    let date = param.year+'-'+param.month
    
    //Get Data Date in month (Ambil semua tanggal di bulan sekarang)
    let allDateMonth = Array.from({length: moment(date).daysInMonth()}, (x, i) => moment(date).startOf('month').add(i, 'days').format('ddd-DD-MM-YYYY'))

    //Filter future Date (filter tanggal sekarang sampai dengan akhir bulan)
    let allDateMonthFuture = []
    allDateMonth.forEach(e => {
      let today = moment(new Date())
      let datePlanning = moment(e, 'ddd-DD-MM-YYYY').format('YYYY-MM-DD')
      let isFuture = today.diff(datePlanning, 'days');
      if (isFuture <= 0){
        allDateMonthFuture.push(e)
      }
    });

    //Filter weekends date (filter weekdays dibulan sekarang)
    let exWeekends = []
    allDateMonthFuture.forEach(e => {
      let split = e.split("-")
      if(split[0] != 'Sat' && split[0] != 'Sun'){
        let format = moment(split[1]+'-'+split[2]+'-'+split[3], 'DD-MM-YYYY').format('YYYY-MM-DD')
        exWeekends.push(format)
      }
    });

    //Get Data Custom Date (Mengambil custom date, untuk pengecualian hari libur custom)
    let search = {
        search: { keyword: date },
        page: 1,
        limit: 100000,
        sortBy: 'gph_date',
        sortOrder: 'ASC'
    }
    const filter = search.search;
    const options = pick(search, ['sortBy','sortOrder', 'limit', 'page']);
    const result = await MonthlyPlanningHolidayService.searchMonthlyPlanningHoliday(filter, options,req.app.locals.userId);
    let dataCustomDate = result.rows

    //Filter data overtime and holiday (memisahkan tanggal weekdays dan weekend pada custom date)
    let holiday = []
    let overtime = []
    dataCustomDate.forEach(e => {
      if(e.GPH_IS_WORK == 'Y'){
        overtime.push(e.GPH_DATE)
      }else if (e.GPH_IS_WORK == 'N'){
        holiday.push(e.GPH_DATE)
      }
    });

    //Filter duplicate data overtime and holiday
    let uniqueHoliday = [...new Set(holiday)]
    let uniqueOvertime = [...new Set(overtime)]


    //Remove holiday date
    let finalDate = exWeekends.filter((value) => {
        return !uniqueHoliday.includes(value)
    })
    //Add overtime date
    finalDate = [...finalDate, ...uniqueOvertime]

    //Final check unique date
    finalDate = [...new Set(finalDate)]

    //Order date
    finalDate.sort(function(a,b){
      return new Date(a) - new Date(b);
    })

    //Filter future Date after final date (tanggal final yang akan dipakai buat generate)
    let finlFutureDate = []
    finalDate.forEach(e => {
      let today = moment(new Date())
      let datePlanning = moment(e, 'YYYY-MM-DD').format('YYYY-MM-DD')
      let isFuture = today.diff(datePlanning, 'days');
      if (isFuture <= 0){
        finlFutureDate.push(e)
      }
    });

    let objBetween = {
      startDate : moment(date, 'YYYY-MM').format('YYYY-MM-DD'),
      endDate : moment(date, 'YYYY-MM').add(1, 'M').format('YYYY-MM-DD')
    }

    //Get data gauge expired in last day month or future month
    let GaugeExp = null
    if (moment(objBetween.startDate).isSame(new Date(), 'month')){
      GaugeExp = await MonthlyPlanningService.getGaugeExpLastDay(objBetween,req.app.locals.userId)
    }else{
      GaugeExp = await MonthlyPlanningService.getGaugeExpMonth(objBetween,req.app.locals.userId)
    }
    let obj = {
      GpDate: date,
      GpDescription: 'Monthly Planning '+ date,
      DataGauges: []
    }


    // Function to distribute gauges evenly based on GAUGE_LEAD_TIME
    function distributeGauges(gauges, dates) {
      // Sort gauges by GAUGE_LEAD_TIME in descending order
      gauges.sort((a, b) => b.GAUGE_LEAD_TIME - a.GAUGE_LEAD_TIME);
      // Initialize an array to hold the distributed gauges for each date
      let distributedGauges = dates.map(date => ({ date, gauges: [], totalLeadTime: 0 }));
      // Distribute gauges to dates in a balanced way
      for (let gauge of gauges) {
        // Find the date with the least total lead time
        distributedGauges.sort((a, b) => a.totalLeadTime - b.totalLeadTime);
        // Assign the gauge to this date
        distributedGauges[0].gauges.push(gauge);
        distributedGauges[0].totalLeadTime += gauge.GAUGE_LEAD_TIME;
      }
      return distributedGauges;
    }

    // Function to distribute gauges evenly based on GAUGE_LEAD_TIME
    function distributeGaugesShift(gauges, shifts) {
      // Sort gauges by GAUGE_LEAD_TIME in descending order
      gauges.sort((a, b) => b.GAUGE_LEAD_TIME - a.GAUGE_LEAD_TIME);
      // Initialize an array to hold the distributed gauges for each date
      let distributedGaugesShift = shifts.map(shift => ({ shift, gauges: [], totalLeadTime: 0 }));
      // Distribute gauges to dates in a balanced way
      for (let gauge of gauges) {
        // Find the date with the least total lead time
        distributedGaugesShift.sort((a, b) => a.totalLeadTime - b.totalLeadTime);
        // Assign the gauge to this date
        distributedGaugesShift[0].gauges.push(gauge);
        distributedGaugesShift[0].totalLeadTime += gauge.GAUGE_LEAD_TIME;
      }
      return distributedGaugesShift;
    }

    let gaugeDistribution = [] 
    if(finlFutureDate.length > 0){
      gaugeDistribution = distributeGauges(GaugeExp[0], finlFutureDate);
    }
    let shifts = [1,2]

    gaugeDistribution.forEach(e => {
      let gaugeDistributionShift = distributeGaugesShift(e.gauges, shifts);
      e.shift_distibution = gaugeDistributionShift
    });
    
    let DataGauges = []
    gaugeDistribution.forEach(e => {
      e.shift_distibution.forEach(e1 => {
        e1.gauges.forEach(e2 => {
          let data = { 
            GpdGaugeId:  e2.GAUGE_ID, 
            GpdDate: e.date,
            GpdShift: e1.shift
          }
          DataGauges.push(data)
        });
      })
    });

    obj.DataGauges = DataGauges

    let detail = obj.DataGauges
    if(detail && detail.length > 0){
      obj.DataGauges = JSON.stringify(detail)
    }else{
      obj.DataGauges = '[]'
    }

    console.log(obj)
    const Generate = await MonthlyPlanningService.createMonthlyPlanning(obj,req.app.locals.userId);
    let response = Generate[0][0].response
    if (response == 200){
      res.send({
        status: httpStatus.OK,
        message: "Successfully generate monthly planning"
      });
    }
  } catch (error) {
    console.log('ERROR GENERATE', error)
    res.send({
      status: httpStatus.INTERNAL_SERVER_ERROR,
      message: "Internal Server Error"
    });
  }

  
});

const createMonthlyPlanningBUFIX = catchAsync(async (req, res) => {

  try {
    let param = req.body
    let date = param.year+'-'+param.month
    
    //Get Data Date in month
    let allDateMonth = Array.from({length: moment(date).daysInMonth()}, (x, i) => moment(date).startOf('month').add(i, 'days').format('ddd-DD-MM-YYYY'))

    //Filter future Date
    let allDateMonthFuture = []
    allDateMonth.forEach(e => {
      let today = moment(new Date())
      let datePlanning = moment(e, 'ddd-DD-MM-YYYY').format('YYYY-MM-DD')
      let isFuture = today.diff(datePlanning, 'days');
      if (isFuture <= 0){
        allDateMonthFuture.push(e)
      }
    });

    //Filter weekends date
    let exWeekends = []
    allDateMonthFuture.forEach(e => {
      let split = e.split("-")
      if(split[0] != 'Sat' && split[0] != 'Sun'){
        let format = moment(split[1]+'-'+split[2]+'-'+split[3], 'DD-MM-YYYY').format('YYYY-MM-DD')
        exWeekends.push(format)
      }
    });

    //Get Data Custom Date
    let search = {
        search: { keyword: date },
        page: 1,
        limit: 100000,
        sortBy: 'gph_date',
        sortOrder: 'ASC'
    }
    const filter = search.search;
    const options = pick(search, ['sortBy','sortOrder', 'limit', 'page']);
    const result = await MonthlyPlanningHolidayService.searchMonthlyPlanningHoliday(filter, options,req.app.locals.userId);
    let dataCustomDate = result.rows

    //Filter data overtime and holiday
    let holiday = []
    let overtime = []
    dataCustomDate.forEach(e => {
      if(e.GPH_IS_WORK == 'Y'){
        overtime.push(e.GPH_DATE)
      }else if (e.GPH_IS_WORK == 'N'){
        holiday.push(e.GPH_DATE)
      }
    });

    //Filter duplicate data overtime and holiday
    let uniqueHoliday = [...new Set(holiday)]
    let uniqueOvertime = [...new Set(overtime)]

    //Remove holiday date
    let finalDate = exWeekends.filter((value) => {
        return !uniqueHoliday.includes(value)
    })
    //Add overtime date
    finalDate = [...finalDate, ...uniqueOvertime]

    //Final check unique date
    finalDate = [...new Set(finalDate)]

    //Order date
    finalDate.sort(function(a,b){
      return new Date(a) - new Date(b);
    })

    //Filter future Date after final date
    let finlFutureDate = []
    finalDate.forEach(e => {
      let today = moment(new Date())
      let datePlanning = moment(e, 'YYYY-MM-DD').format('YYYY-MM-DD')
      let isFuture = today.diff(datePlanning, 'days');
      if (isFuture <= 0){
        finlFutureDate.push(e)
      }
    });

    let objBetween = {
      startDate : moment(date, 'YYYY-MM').format('YYYY-MM-DD'),
      endDate : moment(date, 'YYYY-MM').add(1, 'M').format('YYYY-MM-DD')
    }

    //Get data gauge expired in last day month or future month
    let GaugeExp = null
    if (moment(objBetween.startDate).isSame(new Date(), 'month')){
      GaugeExp = await MonthlyPlanningService.getGaugeExpLastDay(objBetween,req.app.locals.userId)
    }else{
      GaugeExp = await MonthlyPlanningService.getGaugeExpMonth(objBetween,req.app.locals.userId)
    }
    let obj = {
      GpDate: date,
      GpDescription: 'Monthly Planning '+ date,
      DataGauges: []
    }

    //Random generate data
    let gaugesId = []
    GaugeExp[0].forEach(e => {
      gaugesId.push(e.GAUGE_ID)
    })

    function randomizeAndSplit(data, chunkSize) {
      let arrayOfArrays = [];
      let shuffled = [...data];
      //shuffle the elements
      for (let i = shuffled.length - 1; i > 0; i--) {
            const j = Math.floor(Math.random() * (i + 1));
            [shuffled[i], shuffled[j]] = [shuffled[j], shuffled[i]];
      } 
      //split the shuffled version by the chunk size
      for (let i=0; i<shuffled.length; i+=chunkSize) {
        arrayOfArrays.push(shuffled.slice(i,i+chunkSize));
      }
      return arrayOfArrays;
    }

    //Create obj 
    let distributionLenght = Math.ceil(gaugesId.length/finlFutureDate.length)
    let randomData = randomizeAndSplit(gaugesId, distributionLenght)

    for (let i = 0; i < finlFutureDate.length; i++) {
      let dataGauge;
      if(i < randomData.length){
        for (let e = 0; e <= randomData[i].length; e++) {
          dataGauge = {
            GpdGaugeId: randomData[i][e],
            GpdDate: finlFutureDate[i]
          }
          obj.DataGauges.push(dataGauge)
        }
      }
    }

    let filterObj = []
    obj.DataGauges.forEach(e => {
      if(e.GpdGaugeId){
        filterObj.push(e)
      }
    });

    obj.DataGauges = filterObj

    //Insert to DB
    // if(obj.DataGauges.length == 0){
      let detail = obj.DataGauges
      if(detail && detail.length > 0){
        obj.DataGauges = JSON.stringify(detail)
      }else{
        obj.DataGauges = '[]'
      }

      const Generate = await MonthlyPlanningService.createMonthlyPlanning(obj,req.app.locals.userId);
      let response = Generate[0][0].response
      if (response == 200){
        res.send({
          status: httpStatus.OK,
          message: "Successfully generate monthly planning"
        });
      }else{
        res.send({
          status: httpStatus.FORBIDDEN,
          message: "Failed to generate planning"
        });
      }
    // }else{
    //   res.send({
    //     status: httpStatus.FORBIDDEN,
    //     message: "Failed to generate planning [Object null]"
    //   });
    // }
  } catch (error) {
    console.log('ERROR GENERATE', error)
    res.send({
      status: httpStatus.INTERNAL_SERVER_ERROR,
      message: "Internal Server Error"
    });
  }

  
});

const createMonthlyPlanningBU2 = catchAsync(async (req, res) => {

  try {
    let param = req.body
    let date = param.year+'-'+param.month
    
    //Get Data Date in month
    let allDateMonth = Array.from({length: moment(date).daysInMonth()}, (x, i) => moment(date).startOf('month').add(i, 'days').format('ddd-DD-MM-YYYY'))

    //Filter future Date
    let allDateMonthFuture = []
    allDateMonth.forEach(e => {
      let today = moment(new Date())
      let datePlanning = moment(e, 'ddd-DD-MM-YYYY').format('YYYY-MM-DD')
      let isFuture = today.diff(datePlanning, 'days');
      if (isFuture <= 0){
        allDateMonthFuture.push(e)
      }
    });

    //Filter weekends date
    let exWeekends = []
    allDateMonthFuture.forEach(e => {
      let split = e.split("-")
      if(split[0] != 'Sat' && split[0] != 'Sun'){
        let format = moment(split[1]+'-'+split[2]+'-'+split[3], 'DD-MM-YYYY').format('YYYY-MM-DD')
        exWeekends.push(format)
      }
    });

    //Get Data Custom Date
    let search = {
        search: { keyword: date },
        page: 1,
        limit: 100000,
        sortBy: 'gph_date',
        sortOrder: 'ASC'
    }
    const filter = search.search;
    const options = pick(search, ['sortBy','sortOrder', 'limit', 'page']);
    const result = await MonthlyPlanningHolidayService.searchMonthlyPlanningHoliday(filter, options,req.app.locals.userId);
    let dataCustomDate = result.rows

    //Filter data overtime and holiday
    let holiday = []
    let overtime = []
    dataCustomDate.forEach(e => {
      if(e.GPH_IS_WORK == 'Y'){
        overtime.push(e.GPH_DATE)
      }else if (e.GPH_IS_WORK == 'N'){
        holiday.push(e.GPH_DATE)
      }
    });

    //Filter duplicate data overtime and holiday
    let uniqueHoliday = [...new Set(holiday)]
    let uniqueOvertime = [...new Set(overtime)]

    //Remove holiday date
    let finalDate = exWeekends.filter((value) => {
        return !uniqueHoliday.includes(value)
    })
    //Add overtime date
    finalDate = [...finalDate, ...uniqueOvertime]

    //Final check unique date
    finalDate = [...new Set(finalDate)]

    //Order date
    finalDate.sort(function(a,b){
      return new Date(a) - new Date(b);
    })

    //Filter future Date after final date
    let finlFutureDate = []
    finalDate.forEach(e => {
      let today = moment(new Date())
      let datePlanning = moment(e, 'YYYY-MM-DD').format('YYYY-MM-DD')
      let isFuture = today.diff(datePlanning, 'days');
      if (isFuture <= 0){
        finlFutureDate.push(e)
      }
    });

    let objBetween = {
      startDate : moment(date, 'YYYY-MM').format('YYYY-MM-DD'),
      endDate : moment(date, 'YYYY-MM').add(1, 'M').format('YYYY-MM-DD')
    }

    //Get data gauge expired in last day month or future month
    let GaugeExp = null
    if (moment(objBetween.startDate).isSame(new Date(), 'month')){
      GaugeExp = await MonthlyPlanningService.getGaugeExpLastDay(objBetween,req.app.locals.userId)
    }else{
      GaugeExp = await MonthlyPlanningService.getGaugeExpMonth(objBetween,req.app.locals.userId)
    }
    let obj = {
      GpDate: date,
      GpDescription: 'Monthly Planning '+ date,
      DataGauges: []
    }

    //Random generate data
    let gaugesId = []
    GaugeExp[0].forEach(e => {
      gaugesId.push(e.GAUGE_ID)
    })

    function randomizeAndSplit(data, chunkSize) {
      let arrayOfArrays = [];
      let shuffled = [...data];
      //shuffle the elements
      for (let i = shuffled.length - 1; i > 0; i--) {
            const j = Math.floor(Math.random() * (i + 1));
            [shuffled[i], shuffled[j]] = [shuffled[j], shuffled[i]];
      } 
      //split the shuffled version by the chunk size
      for (let i=0; i<shuffled.length; i+=chunkSize) {
        arrayOfArrays.push(shuffled.slice(i,i+chunkSize));
      }
      return arrayOfArrays;
    }

    //Create obj 
    let distributionLenght = Math.ceil(gaugesId.length/finlFutureDate.length)
    let randomData = randomizeAndSplit(gaugesId, distributionLenght)
    for (let i = 0; i < finlFutureDate.length; i++) {
      let dataGauge;
      if(i < randomData.length){
        for (let e = 0; e <= randomData[i].length; e++) {
          dataGauge = {
            GpdGaugeId: randomData[i][e],
            GpdDate: finlFutureDate[i]
          }
          obj.DataGauges.push(dataGauge)
        }
      }
    }

    let filterObj = []
    obj.DataGauges.forEach(e => {
      if(e.GpdGaugeId){
        filterObj.push(e)
      }
    });

    obj.DataGauges = filterObj

    //Insert to DB
    if(obj.DataGauges.length > 0){
      let detail = obj.DataGauges
      if(detail && detail.length > 0){
        obj.DataGauges = JSON.stringify(detail)
      }else{
        obj.DataGauges = '[]'
      }

      const Generate = await MonthlyPlanningService.createMonthlyPlanning(obj,req.app.locals.userId);
      let response = Generate[0][0].response
      if (response == 200){
        res.send({
          status: httpStatus.OK,
          message: "Successfully generate monthly planning"
        });
      }else{
        res.send({
          status: httpStatus.FORBIDDEN,
          message: "Failed to generate planning"
        });
      }
    }else{
      res.send({
        status: httpStatus.FORBIDDEN,
        message: "Failed to generate planning [Object null]"
      });
    }
  } catch (error) {
    console.log('ERROR GENERATE', error)
    res.send({
      status: httpStatus.INTERNAL_SERVER_ERROR,
      message: "Internal Server Error"
    });
  }

  
});

const createMonthlyPlanningBU = catchAsync(async (req, res) => {

  let param = req.body
  let date = param.year+'-'+param.month
  
  //Get Data Date in month
  let allDateMonth = Array.from({length: moment(date).daysInMonth()}, (x, i) => moment(date).startOf('month').add(i, 'days').format('ddd-DD-MM-YYYY'))

  //Filter future Date
  let allDateMonthFuture = []
  allDateMonth.forEach(e => {
    let today = moment(new Date())
    let datePlanning = moment(e, 'ddd-DD-MM-YYYY').format('YYYY-MM-DD')
    let isFuture = today.diff(datePlanning, 'days');
    if (isFuture <= 0){
      allDateMonthFuture.push(e)
    }
  });

  //Filter weekends date
  let exWeekends = []
  allDateMonthFuture.forEach(e => {
    let split = e.split("-")
    if(split[0] != 'Sat' && split[0] != 'Sun'){
      let format = moment(split[1]+'-'+split[2]+'-'+split[3], 'DD-MM-YYYY').format('YYYY-MM-DD')
      exWeekends.push(format)
    }
  });

  //Get Data Custom Date
  let search = {
      search: { keyword: date },
      page: 1,
      limit: 100000,
      sortBy: 'gph_date',
      sortOrder: 'ASC'
  }
  const filter = search.search;
  const options = pick(search, ['sortBy','sortOrder', 'limit', 'page']);
  const result = await MonthlyPlanningHolidayService.searchMonthlyPlanningHoliday(filter, options,req.app.locals.userId);
  let dataCustomDate = result.rows

  //Filter data overtime and holiday
  let holiday = []
  let overtime = []
  dataCustomDate.forEach(e => {
    if(e.GPH_IS_WORK == 'Y'){
      overtime.push(e.GPH_DATE)
    }else if (e.GPH_IS_WORK == 'N'){
      holiday.push(e.GPH_DATE)
    }
  });

  //Filter duplicate data overtime and holiday
  let uniqueHoliday = [...new Set(holiday)]
  let uniqueOvertime = [...new Set(overtime)]

  //Remove holiday date
  let finalDate = exWeekends.filter((value) => {
      return !uniqueHoliday.includes(value)
  })
  //Add overtime date
  finalDate = [...finalDate, ...uniqueOvertime]

  //Final check unique date
  finalDate = [...new Set(finalDate)]

  //Order date
  finalDate.sort(function(a,b){
    return new Date(a) - new Date(b);
  })

  //Filter future Date after final date
  let finlFutureDate = []
  finalDate.forEach(e => {
    let today = moment(new Date())
    let datePlanning = moment(e, 'YYYY-MM-DD').format('YYYY-MM-DD')
    let isFuture = today.diff(datePlanning, 'days');
    if (isFuture <= 0){
      finlFutureDate.push(e)
    }
  });

  //Get data gauge expired in last day month
  const GaugeExp = await MonthlyPlanningService.getGaugeExpLastDay({},req.app.locals.userId)
  let obj = {
    GpDate: date,
    GpDescription: 'Monthly Planning '+ date,
    DataGauges: []
  }

  //Random generate data
  let gaugesId = []
  GaugeExp[0].forEach(e => {
    gaugesId.push(e.GAUGE_ID)
  })

  function randomizeAndSplit(data, chunkSize) {
    let arrayOfArrays = [];
    let shuffled = [...data];
    //shuffle the elements
    for (let i = shuffled.length - 1; i > 0; i--) {
          const j = Math.floor(Math.random() * (i + 1));
          [shuffled[i], shuffled[j]] = [shuffled[j], shuffled[i]];
    } 
    //split the shuffled version by the chunk size
    for (let i=0; i<shuffled.length; i+=chunkSize) {
       arrayOfArrays.push(shuffled.slice(i,i+chunkSize));
    }
    return arrayOfArrays;
  }

  //Create obj 
  let distributionLenght = Math.ceil(gaugesId.length/finlFutureDate.length)
  let randomData = randomizeAndSplit(gaugesId, distributionLenght)
  for (let i = 0; i < finlFutureDate.length; i++) {
    let dataGauge;
    for (let e = 0; e < randomData[i].length; e++) {
      dataGauge = {
        GpdGaugeId: randomData[i][e],
        GpdDate: finlFutureDate[i]
      }
      obj.DataGauges.push(dataGauge)
    }
  }

  //Insert to DB
  if(obj.DataGauges.length > 0){
    let detail = obj.DataGauges
    if(detail && detail.length > 0){
      obj.DataGauges = JSON.stringify(detail)
    }else{
      obj.DataGauges = '[]'
    }

    const Generate = await MonthlyPlanningService.createMonthlyPlanning(obj,req.app.locals.userId);
    let response = Generate[0][0].response
    if (response == 200){
      res.send({
        status: httpStatus.OK,
        message: "Successfully generate monthly planning"
      });
    }else{
      res.send({
        status: httpStatus.FORBIDDEN,
        message: "Failed to generate planning"
      });
    }
  }else{
    res.send({
      status: httpStatus.FORBIDDEN,
      message: "Failed to generate planning [Object null]"
    });
  }
});

const getMonthlyPlanning = catchAsync(async (req, res) => {
  const Check = await MonthlyPlanningService.getMonthlyPlanning(req.body,req.app.locals.userId);
  if (Check[0].length == 0) {
    res.send({
      status:httpStatus.OK,
      message: 'Monthly Planning Value Item not found',
      data: null,
      details: null
    });
  }else{
    res.send({
      status:httpStatus.OK,
      message: null,
      data: Check[0],
      details: null
    });
  }
});

const getMonthlyPlanningLineReport = catchAsync(async (req, res) => {
  try {

    let body = req.body
    console.log(body)
    
    const result = await MonthlyPlanningService.getMonthlyPlanningLineReport(body,req.app.locals.userId);
    let gaugePlanning = result[0]
    if (gaugePlanning.length == 0){

      let filePath = __basedir + "/src/template/TEMPLATE_REPORT_MONTHLY_GAUGE_[NO_DATA].xlsx"
      const workbook = new excel.Workbook();
      await workbook.xlsx.readFile(filePath).then(async () => {
        let ws = workbook.getWorksheet('REPORT');
        let dateReport = body.month + '-' + body.year 
        ws.getCell('E6').value = `RESUME KALIBRASI ALAT UKUR BULAN ${moment(dateReport, 'MM-YYYY').format('MMMM YYYY').toUpperCase()}`;

        }).catch(err => {
          console.log(err.message);
        });

      res.setHeader(
        "Content-Type",
        "application/vnd.openxmlformats-officedocument.spreadsheetml.sheet"
      );
      res.setHeader(
        "Content-Disposition",
        "attachment; filename=" + `REPORT_MONTHLY_PLANNING [${moment(body.month + '-' + body.year , 'MM-YYYY').format('MMMM YYYY').toUpperCase()}]_${moment().unix()}.xlsx`
      );
      await workbook.xlsx.write(res)
        .then(async function (data) {
          res.end();
          console.log('File write done........');
        });

    }else{

      let filePath = __basedir + "/src/template/TEMPLATE_REPORT_MONTHLY_GAUGE.xlsx"
      const workbook = new excel.Workbook();
      await workbook.xlsx.readFile(filePath).then(async () => {
        
        let imgChartFreq = null
        let imgChartMonth = null
        if (body.imgFreq != ''){
          imgChartFreq = workbook.addImage({
            base64: body.imgFreq,
            extension: 'png',
          });
        }

        if(body.imgMonth != ''){
          imgChartMonth = workbook.addImage({
            base64: body.imgMonth,
            extension: 'png',
          });
        }


        let ws = workbook.getWorksheet('REPORT');
       
        if (gaugePlanning.length > 0) {
  
          //Style
          var borderStyles = {
            top: { style: "thin" },
            left: { style: "thin" },
            bottom: { style: "thin" },
            right: { style: "thin" }
          };
          var alignmentStyles = { vertical: 'middle', horizontal: 'center' };
          var fontStyles = {
            name: 'Calibri',
            family: 4,
            size: 12,
            bold: true,
            // italic: true,
            // color: {argb: "004e47cc"}
          };
          var fontTitleStyles = {
            name: 'Calibri',
            family: 4,
            size: 16,
            bold: true,
          };
          var fontSubTitleStyles = {
            name: 'Calibri',
            family: 4,
            size: 11,
            bold: true,
          };
          var fillStyles = {
              type: "pattern",
              pattern: "solid",
              fgColor: { argb: "2563EB" },
          }
  
          // Data
          let i = 0;
          let total = {
            totalR_1M_PLAN : 0,
            totalR_1M_DO_OK : 0,
            totalR_1M_DO_NG : 0,
            totalR_1_3M_PLAN : 0,
            totalR_1_3M_DO_OK : 0,
            totalR_1_3M_DO_NG : 0,
            totalR_1_6M_PLAN : 0,
            totalR_1_6M_DO_OK : 0,
            totalR_1_6M_DO_NG : 0,
            totalR_1Y_PLAN : 0,
            totalR_1Y_DO_OK : 0,
            totalR_1Y_DO_NG : 0,
            totalR_1_2Y_PLAN : 0,
            totalR_1_2Y_DO_OK : 0,
            totalR_1_2Y_DO_NG : 0,
            totalR_UNPLANNING : 0,
            totalR_TG_PLAN : 0,
            totalR_TG_DO_OK : 0,
            totalR_TG_DO_NG : 0
          }
        
          for (i = 0; i < gaugePlanning.length; i++) {
            ws.getCell('C' + (i+13)).value = i+1;
            ws.getCell('D' + (i+13)).value = gaugePlanning[i].LINE_NAME;
            ws.getCell('E' + (i+13)).value = gaugePlanning[i].R_1M_PLAN;
            ws.getCell('F' + (i+13)).value = gaugePlanning[i].R_1M_DO_OK;
            ws.getCell('G' + (i+13)).value = gaugePlanning[i].R_1M_DO_NG;
            ws.getCell('H' + (i+13)).value = gaugePlanning[i].R_1_3M_PLAN;
            ws.getCell('I' + (i+13)).value = gaugePlanning[i].R_1_3M_DO_OK;
            ws.getCell('J' + (i+13)).value = gaugePlanning[i].R_1_3M_DO_NG;
            ws.getCell('K' + (i+13)).value = gaugePlanning[i].R_1_6M_PLAN;
            ws.getCell('L' + (i+13)).value = gaugePlanning[i].R_1_6M_DO_OK;
            ws.getCell('M' + (i+13)).value = gaugePlanning[i].R_1_6M_DO_NG;
            ws.getCell('N' + (i+13)).value = gaugePlanning[i].R_1Y_PLAN;
            ws.getCell('O' + (i+13)).value = gaugePlanning[i].R_1Y_DO_OK;
            ws.getCell('P' + (i+13)).value = gaugePlanning[i].R_1Y_DO_NG;
            ws.getCell('Q' + (i+13)).value = gaugePlanning[i].R_1_2Y_PLAN;
            ws.getCell('R' + (i+13)).value = gaugePlanning[i].R_1_2Y_DO_OK;
            ws.getCell('S' + (i+13)).value = gaugePlanning[i].R_1_2Y_DO_NG;
            ws.getCell('T' + (i+13)).value = '';
            ws.getCell('U' + (i+13)).value = gaugePlanning[i].R_TG_PLAN;
            ws.getCell('V' + (i+13)).value = gaugePlanning[i].R_TG_DO_OK;
            ws.getCell('W' + (i+13)).value = gaugePlanning[i].R_TG_DO_NG;
  
            ws.getCell('C' + (i+13)).alignment = alignmentStyles;
            ws.getCell('E' + (i+13)).alignment = alignmentStyles;
            ws.getCell('F' + (i+13)).alignment = alignmentStyles;
            ws.getCell('G' + (i+13)).alignment = alignmentStyles;
            ws.getCell('H' + (i+13)).alignment = alignmentStyles;
            ws.getCell('I' + (i+13)).alignment = alignmentStyles;
            ws.getCell('J' + (i+13)).alignment = alignmentStyles;
            ws.getCell('K' + (i+13)).alignment = alignmentStyles;
            ws.getCell('L' + (i+13)).alignment = alignmentStyles;
            ws.getCell('M' + (i+13)).alignment = alignmentStyles;
            ws.getCell('N' + (i+13)).alignment = alignmentStyles;
            ws.getCell('O' + (i+13)).alignment = alignmentStyles;
            ws.getCell('P' + (i+13)).alignment = alignmentStyles;
            ws.getCell('Q' + (i+13)).alignment = alignmentStyles;
            ws.getCell('R' + (i+13)).alignment = alignmentStyles;
            ws.getCell('S' + (i+13)).alignment = alignmentStyles;
            ws.getCell('T' + (i+13)).alignment = alignmentStyles;
            ws.getCell('U' + (i+13)).alignment = alignmentStyles;
            ws.getCell('V' + (i+13)).alignment = alignmentStyles;
            ws.getCell('W' + (i+13)).alignment = alignmentStyles;
  
            ws.getCell('C' + (i+13)).border = borderStyles;
            ws.getCell('D' + (i+13)).border = borderStyles;
            ws.getCell('E' + (i+13)).border = borderStyles;
            ws.getCell('F' + (i+13)).border = borderStyles;
            ws.getCell('G' + (i+13)).border = borderStyles;
            ws.getCell('H' + (i+13)).border = borderStyles;
            ws.getCell('I' + (i+13)).border = borderStyles;
            ws.getCell('J' + (i+13)).border = borderStyles;
            ws.getCell('K' + (i+13)).border = borderStyles;
            ws.getCell('L' + (i+13)).border = borderStyles;
            ws.getCell('M' + (i+13)).border = borderStyles;
            ws.getCell('N' + (i+13)).border = borderStyles;
            ws.getCell('O' + (i+13)).border = borderStyles;
            ws.getCell('P' + (i+13)).border = borderStyles;
            ws.getCell('Q' + (i+13)).border = borderStyles;
            ws.getCell('R' + (i+13)).border = borderStyles;
            ws.getCell('S' + (i+13)).border = borderStyles;
            ws.getCell('T' + (i+13)).border = borderStyles;
            ws.getCell('U' + (i+13)).border = borderStyles;
            ws.getCell('V' + (i+13)).border = borderStyles;
            ws.getCell('W' + (i+13)).border = borderStyles;
  
            total.totalR_1M_PLAN += gaugePlanning[i].R_1M_PLAN;
            total.totalR_1M_DO_OK += gaugePlanning[i].R_1M_DO_OK;
            total.totalR_1M_DO_NG += gaugePlanning[i].R_1M_DO_NG;
            total.totalR_1_3M_PLAN += gaugePlanning[i].R_1_3M_PLAN;
            total.totalR_1_3M_DO_OK += gaugePlanning[i].R_1_3M_DO_OK;
            total.totalR_1_3M_DO_NG += gaugePlanning[i].R_1_3M_DO_NG;
            total.totalR_1_6M_PLAN += gaugePlanning[i].R_1_6M_PLAN;
            total.totalR_1_6M_DO_OK += gaugePlanning[i].R_1_6M_DO_OK;
            total.totalR_1_6M_DO_NG += gaugePlanning[i].R_1_6M_DO_NG;
            total.totalR_1Y_PLAN += gaugePlanning[i].R_1Y_PLAN;
            total.totalR_1Y_DO_OK += gaugePlanning[i].R_1Y_DO_OK;
            total.totalR_1Y_DO_NG += gaugePlanning[i].R_1Y_DO_NG;
            total.totalR_1_2Y_PLAN += gaugePlanning[i].R_1_2Y_PLAN;
            total.totalR_1_2Y_DO_OK += gaugePlanning[i].R_1_2Y_DO_OK;
            total.totalR_1_2Y_DO_NG += gaugePlanning[i].R_1_2Y_DO_NG;
            total.totalR_UNPLANNING += 0;
            total.totalR_TG_PLAN += gaugePlanning[i].R_TG_PLAN;
            total.totalR_TG_DO_OK += gaugePlanning[i].R_TG_DO_OK;
            total.totalR_TG_DO_NG += gaugePlanning[i].R_TG_DO_NG;
          }
            
            ws.getCell('Y9').border = borderStyles;
            ws.mergeCells('Y9:AD25');
            if(body.imgMonth != ''){
              ws.addImage(imgChartMonth, 'Y9:AD25');
            }

            let dateReport = body.month + '-' + body.year 
            ws.getCell('E6').value = `RESUME KALIBRASI ALAT UKUR BULAN ${moment(dateReport, 'MM-YYYY').format('MMMM YYYY').toUpperCase()}`;
            ws.getCell('D' + (i+13)).value = 'Total';
            ws.getCell('E' + (i+13)).value = total.totalR_1M_PLAN;
            ws.getCell('F' + (i+13)).value = total.totalR_1M_DO_OK;
            ws.getCell('G' + (i+13)).value = total.totalR_1M_DO_NG;
            ws.getCell('H' + (i+13)).value = total.totalR_1_3M_PLAN;
            ws.getCell('I' + (i+13)).value = total.totalR_1_3M_DO_OK;
            ws.getCell('J' + (i+13)).value = total.totalR_1_3M_DO_NG;
            ws.getCell('K' + (i+13)).value = total.totalR_1_6M_PLAN;
            ws.getCell('L' + (i+13)).value = total.totalR_1_6M_DO_OK;
            ws.getCell('M' + (i+13)).value = total.totalR_1_6M_DO_NG;
            ws.getCell('N' + (i+13)).value = total.totalR_1Y_PLAN;
            ws.getCell('O' + (i+13)).value = total.totalR_1Y_DO_OK;
            ws.getCell('P' + (i+13)).value = total.totalR_1Y_DO_NG;
            ws.getCell('Q' + (i+13)).value = total.totalR_1_2Y_PLAN;
            ws.getCell('R' + (i+13)).value = total.totalR_1_2Y_DO_OK;
            ws.getCell('S' + (i+13)).value = total.totalR_1_2Y_DO_NG;
            ws.getCell('T' + (i+13)).value = total.totalR_UNPLANNING;
            ws.getCell('U' + (i+13)).value = total.totalR_TG_PLAN;
            ws.getCell('V' + (i+13)).value = total.totalR_TG_DO_OK;
            ws.getCell('W' + (i+13)).value = total.totalR_TG_DO_NG;
  
            ws.getCell('E6').font = fontTitleStyles;
            ws.getCell('D' + (i+13)).font = fontStyles;
            ws.getCell('E' + (i+13)).font = fontStyles;
            ws.getCell('F' + (i+13)).font = fontStyles;
            ws.getCell('G' + (i+13)).font = fontStyles;
            ws.getCell('H' + (i+13)).font = fontStyles;
            ws.getCell('I' + (i+13)).font = fontStyles;
            ws.getCell('J' + (i+13)).font = fontStyles;
            ws.getCell('K' + (i+13)).font = fontStyles;
            ws.getCell('L' + (i+13)).font = fontStyles;
            ws.getCell('M' + (i+13)).font = fontStyles;
            ws.getCell('N' + (i+13)).font = fontStyles;
            ws.getCell('O' + (i+13)).font = fontStyles;
            ws.getCell('P' + (i+13)).font = fontStyles;
            ws.getCell('Q' + (i+13)).font = fontStyles;
            ws.getCell('R' + (i+13)).font = fontStyles;
            ws.getCell('S' + (i+13)).font = fontStyles;
            ws.getCell('T' + (i+13)).font = fontStyles;
            ws.getCell('U' + (i+13)).font = fontStyles;
            ws.getCell('V' + (i+13)).font = fontStyles;
            ws.getCell('W' + (i+13)).font = fontStyles;
            
            ws.getCell('E6').alignment = alignmentStyles;
            ws.getCell('E' + (i+13)).alignment = alignmentStyles;
            ws.getCell('F' + (i+13)).alignment = alignmentStyles;
            ws.getCell('G' + (i+13)).alignment = alignmentStyles;
            ws.getCell('H' + (i+13)).alignment = alignmentStyles;
            ws.getCell('I' + (i+13)).alignment = alignmentStyles;
            ws.getCell('J' + (i+13)).alignment = alignmentStyles;
            ws.getCell('K' + (i+13)).alignment = alignmentStyles;
            ws.getCell('L' + (i+13)).alignment = alignmentStyles;
            ws.getCell('M' + (i+13)).alignment = alignmentStyles;
            ws.getCell('N' + (i+13)).alignment = alignmentStyles;
            ws.getCell('O' + (i+13)).alignment = alignmentStyles;
            ws.getCell('P' + (i+13)).alignment = alignmentStyles;
            ws.getCell('Q' + (i+13)).alignment = alignmentStyles;
            ws.getCell('R' + (i+13)).alignment = alignmentStyles;
            ws.getCell('S' + (i+13)).alignment = alignmentStyles;
            ws.getCell('T' + (i+13)).alignment = alignmentStyles;
            ws.getCell('U' + (i+13)).alignment = alignmentStyles;
            ws.getCell('V' + (i+13)).alignment = alignmentStyles;
            ws.getCell('W' + (i+13)).alignment = alignmentStyles;
  
            ws.getCell('C' + (i+13)).border = borderStyles;
            ws.getCell('D' + (i+13)).border = borderStyles;
            ws.getCell('E' + (i+13)).border = borderStyles;
            ws.getCell('F' + (i+13)).border = borderStyles;
            ws.getCell('G' + (i+13)).border = borderStyles;
            ws.getCell('H' + (i+13)).border = borderStyles;
            ws.getCell('I' + (i+13)).border = borderStyles;
            ws.getCell('J' + (i+13)).border = borderStyles;
            ws.getCell('K' + (i+13)).border = borderStyles;
            ws.getCell('L' + (i+13)).border = borderStyles;
            ws.getCell('M' + (i+13)).border = borderStyles;
            ws.getCell('N' + (i+13)).border = borderStyles;
            ws.getCell('O' + (i+13)).border = borderStyles;
            ws.getCell('P' + (i+13)).border = borderStyles;
            ws.getCell('Q' + (i+13)).border = borderStyles;
            ws.getCell('R' + (i+13)).border = borderStyles;
            ws.getCell('S' + (i+13)).border = borderStyles;
            ws.getCell('T' + (i+13)).border = borderStyles;
            ws.getCell('U' + (i+13)).border = borderStyles;
            ws.getCell('V' + (i+13)).border = borderStyles;
            ws.getCell('W' + (i+13)).border = borderStyles;
            
            let e = i+15
            e < 28 ? e = 28 : null
  
            //Field graph actual calibration
            // ws.mergeCells('C' + (e) + ':' + 'J' + (e+1));
            // ws.getCell('C' + (e)).value = `Aktual Kalibrasi`;
            // ws.getCell('C' + (e)).alignment = alignmentStyles;
            // ws.getCell('C' + (e)).font = fontSubTitleStyles;
            // ws.mergeCells('C' + (e+2) + ':' + 'J' + (e+14)); // image graph here
            // ws.getCell('C' + (e)).border = borderStyles;
            // ws.getCell('C' + (e+2)).border = borderStyles;
            ws.getCell('C' + (e)).border = borderStyles;
            ws.mergeCells('C' + (e) + ':' + 'J' + (e+21));
            if(body.imgMonth != ''){
              ws.addImage(imgChartFreq, 'C' + (e) + ':' + 'J' + (e+21));
            }
  
            //Title MG Gauge Table
            ws.mergeCells('L' + (e) + ':' + 'AD' + (e));
            ws.getCell('L' + (e)).value = `PERLAKUAN UNTUK GAUGE YANG NG`;
            ws.getRow(e).height = 25;
            ws.getCell('L' + (e)).alignment = alignmentStyles;
            ws.getCell('L' + (e)).font = fontSubTitleStyles;
  
            //Header NG Gauge Table
            ws.mergeCells('M' + (e+1) + ':' + 'Q' + (e+1));
            ws.mergeCells('R' + (e+1) + ':' + 'S' + (e+1));
            ws.mergeCells('T' + (e+1) + ':' + 'X' + (e+1));
            ws.mergeCells('Y' + (e+1) + ':' + 'Z' + (e+1));
            ws.mergeCells('AA' + (e+1) + ':' + 'AC' + (e+1));
  
            ws.getCell('L' + (e+1)).value = `No`;
            ws.getCell('M' + (e+1)).value = `Gauge NG`;
            ws.getCell('R' + (e+1)).value = `Freq`;
            ws.getCell('T' + (e+1)).value = `Problem`;
            ws.getCell('Y' + (e+1)).value = `Action`;
            ws.getCell('AA' + (e+1)).value = `Konfirmasi Unit Problem`;
            ws.getCell('AD' + (e+1)).value = `Judge`;
            
            ws.getCell('L' + (e+1)).font = fontSubTitleStyles;
            ws.getCell('M' + (e+1)).font = fontSubTitleStyles;
            ws.getCell('R' + (e+1)).font = fontSubTitleStyles;
            ws.getCell('T' + (e+1)).font = fontSubTitleStyles;
            ws.getCell('Y' + (e+1)).font = fontSubTitleStyles;
            ws.getCell('AA' + (e+1)).font = fontSubTitleStyles;
            ws.getCell('AD' + (e+1)).font = fontSubTitleStyles;
  
            ws.getCell('L' + (e+1)).alignment = alignmentStyles;
            ws.getCell('M' + (e+1)).alignment = alignmentStyles;
            ws.getCell('R' + (e+1)).alignment = alignmentStyles;
            ws.getCell('T' + (e+1)).alignment = alignmentStyles;
            ws.getCell('Y' + (e+1)).alignment = alignmentStyles;
            ws.getCell('AA' + (e+1)).alignment = alignmentStyles;
            ws.getCell('AD' + (e+1)).alignment = alignmentStyles;
  
            ws.getCell('L' + (e+1)).border = borderStyles;
            ws.getCell('M' + (e+1)).border = borderStyles;
            ws.getCell('R' + (e+1)).border = borderStyles;
            ws.getCell('T' + (e+1)).border = borderStyles;
            ws.getCell('Y' + (e+1)).border = borderStyles;
            ws.getCell('AA' + (e+1)).border = borderStyles;
            ws.getCell('AD' + (e+1)).border = borderStyles;
  
            let lastIdx = e+2
            let idx = 0
  
            const resultNg = await MonthlyPlanningService.getMonthlyPlanningNgReport(body,req.app.locals.userId);
            let ngGauges = resultNg[0]
  
            for (idx = 0; idx < ngGauges.length; idx++) {
  
              ws.mergeCells('M' + (idx+lastIdx) + ':' + 'Q' + (idx+lastIdx));
              ws.mergeCells('R' + (idx+lastIdx) + ':' + 'S' + (idx+lastIdx));
              ws.mergeCells('T' + (idx+lastIdx) + ':' + 'X' + (idx+lastIdx));
              ws.mergeCells('Y' + (idx+lastIdx) + ':' + 'Z' + (idx+lastIdx));
              ws.mergeCells('AA' + (idx+lastIdx) + ':' + 'AC' + (idx+lastIdx));
  
              ws.getCell('L' + (idx+lastIdx)).value = idx+1;
              ws.getCell('M' + (idx+lastIdx)).value = ngGauges[idx].GAUGE_SERIAL_NO;
              ws.getCell('R' + (idx+lastIdx)).value = 1;
              ws.getCell('T' + (idx+lastIdx)).value = null;
              ws.getCell('Y' + (idx+lastIdx)).value = ngGauges[idx].GAUGE_REMARK;
              ws.getCell('AA' + (idx+lastIdx)).value = null;
              ws.getCell('AD' + (idx+lastIdx)).value = ngGauges[idx].GAUGE_LAST_JUDGEMENT;
  
              ws.getCell('L' + (idx+lastIdx)).alignment = alignmentStyles;
              ws.getCell('R' + (idx+lastIdx)).alignment = alignmentStyles;
              ws.getCell('AD' + (idx+lastIdx)).alignment = alignmentStyles;
  
              ws.getCell('L' + (idx+lastIdx)).border = borderStyles;
              ws.getCell('M' + (idx+lastIdx)).border = borderStyles;
              ws.getCell('R' + (idx+lastIdx)).border = borderStyles;
              ws.getCell('T' + (idx+lastIdx)).border = borderStyles;
              ws.getCell('Y' + (idx+lastIdx)).border = borderStyles;
              ws.getCell('AA' + (idx+lastIdx)).border = borderStyles;
              ws.getCell('AD' + (idx+lastIdx)).border = borderStyles;
            }
  
            // Approval
            let eAp = idx + lastIdx + 2
            eAp < 45 ? eAp = 45 : null
  
            ws.mergeCells('Y' + (eAp) + ':' + 'Z' + (eAp));
            ws.mergeCells('AA' + (eAp) + ':' + 'AB' + (eAp));
            ws.mergeCells('AC' + (eAp) + ':' + 'AD' + (eAp));
            ws.mergeCells('Y' + (eAp+1) + ':' + 'Z' + (eAp+6));
            ws.mergeCells('AA' + (eAp+1) + ':' + 'AB' + (eAp+6));
            ws.mergeCells('AC' + (eAp+1) + ':' + 'AD' + (eAp+6));
            ws.mergeCells('Y' + (eAp+7) + ':' + 'Z' + (eAp+7));
            ws.mergeCells('AA' + (eAp+7) + ':' + 'AB' + (eAp+7));
            ws.mergeCells('AC' + (eAp+7) + ':' + 'AD' + (eAp+7));
  
            ws.getCell('Y' + (eAp)).value = `APPROVED`;
            ws.getCell('AA' + (eAp)).value = `CHECKED`;
            ws.getCell('AC' + (eAp)).value = `PREPARED`;
  
            ws.getCell('Y' + (eAp+7)).value = `Sect / H`;
            ws.getCell('AA' + (eAp+7)).value = `L / H`;
            ws.getCell('AC' + (eAp+7)).value = `G / H`;
  
            ws.getCell('Y' + (eAp)).border = borderStyles;
            ws.getCell('AA' + (eAp)).border = borderStyles;
            ws.getCell('AC' + (eAp)).border = borderStyles;
            ws.getCell('Y' + (eAp+1)).border = borderStyles;
            ws.getCell('AA' + (eAp+1)).border = borderStyles;
            ws.getCell('AC' + (eAp+1)).border = borderStyles;
            ws.getCell('Y' + (eAp+7)).border = borderStyles;
            ws.getCell('AA' + (eAp+7)).border = borderStyles;
            ws.getCell('AC' + (eAp+7)).border = borderStyles;
  
            ws.getCell('Y' + (eAp)).alignment = alignmentStyles;
            ws.getCell('AA' + (eAp)).alignment = alignmentStyles;
            ws.getCell('AC' + (eAp)).alignment = alignmentStyles;
            ws.getCell('Y' + (eAp+7)).alignment = alignmentStyles;
            ws.getCell('AA' + (eAp+7)).alignment = alignmentStyles;
            ws.getCell('AC' + (eAp+7)).alignment = alignmentStyles;
  
        }
  
      }).catch(err => {
        console.log(err.message);
      });
  
      res.setHeader(
        "Content-Type",
        "application/vnd.openxmlformats-officedocument.spreadsheetml.sheet"
      );
      res.setHeader(
        "Content-Disposition",
        "attachment; filename=" + `REPORT_MONTHLY_PLANNING [${moment(body.month + '-' + body.year , 'MM-YYYY').format('MMMM YYYY').toUpperCase()}]_${moment().unix()}.xlsx`
      );
      await workbook.xlsx.write(res)
        .then(async function (data) {
          res.end();
          console.log('File write done........');
        });
    }

  } catch (e) {
    console.log('ERROR CATCH', e)
  }
});

module.exports = {
  searchMonthlyPlanning,
  createMonthlyPlanning,
  getMonthlyPlanning,
  getMonthlyPlanningLineReport
};

const httpStatus = require('http-status');
const pick = require('../../utils/pick');
const ApiError = require('../../utils/ApiError');
const catchAsync = require('../../utils/catchAsync');
const { CalibrationService } = require('../../services');
const CommonAPI = require('../../utils/CommonAPI');
const moment = require('moment');

const createCalibration = catchAsync(async (req, res) => {
    
    let calibrationValue = req.body.calibration_value
    if(calibrationValue && calibrationValue.length > 0){
      req.body.calibration_value = JSON.stringify(calibrationValue)
    }else{
      req.body.calibration_value = '[]'
    }
    console.log(req.body)

    const Calibration = await CalibrationService.createCalibration(req.body,req.app.locals.userId);
    let response = Calibration[0][0].response
    if (response == 200){
      res.send({
        status: httpStatus.OK,
        message: "Successfully inserted data Calibration"
      });
    }else{
      res.send({
        status: httpStatus.FORBIDDEN,
        message: "Failed to insert data Calibration"
      });
    }
});

const updateCalibration = catchAsync(async (req, res) => {
  try {
    await CalibrationService.updateCalibration(req.body,req.app.locals.userId);
    res.send({
        status:httpStatus.OK,
        message:"Calibration successfuly updated",
    }); 
  } catch (error) {
    res.send({
      status: httpStatus.FORBIDDEN,
      message: "Failed to update Calibration"
    });
  }
});

const updateCalibrationStatus = catchAsync(async (req, res) => {
  try {
    await CalibrationService.updateCalibrationStatus(req.body,req.app.locals.userId);
    res.send({
        status:httpStatus.OK,
        message:"Calibration successfuly updated",
    }); 
  } catch (error) {
    res.send({
      status: httpStatus.FORBIDDEN,
      message: "Failed to update Calibration"
    });
  }
});

const searchCalibration = catchAsync(async (req, res) => {
  const filter = req.body.search;
  const options = pick(req.body, ['sortBy','sortOrder', 'limit', 'page']);
  const result = await CalibrationService.queryCal(filter, options,req.app.locals.userId);
  res.send({
    status:httpStatus.OK,
    message:null,
    details:null,
    data:result.rows,
    totalRecords:result.count,
    totalPages:Math.ceil(result.count/req.body.limit),
    pageNo:req.body.page,
    pageSize:req.body.limit
  });
});

const getCalibrationValue = catchAsync(async (req, res) => {
  const CalValue = await CalibrationService.getCalibrationValue(req.body,req.app.locals.userId);
  if (CalValue[0].length == 0) {
    res.send({
      status:httpStatus.OK,
      message: 'Calibration Value Item not found',
      data: null,
      details: null
    });
  }else{
    res.send({
      status:httpStatus.OK,
      message: null,
      data: CalValue[0],
      details: null
    });
  }
});

const deleteCalibration = catchAsync(async (req, res) => {
    await CalibrationService.deleteCalibration(req.body,req.app.locals.userId);
    res.send({
      status:httpStatus.OK,
      message:"Calibration successfuly deleted",
    });
});

const createCalibrationDraft = catchAsync(async (req, res) => {

  const Calibration = await CalibrationService.createCalibrationDraft(req.body,req.app.locals.userId);
  let response = Calibration[0][0].response
  if (response == 200){
    res.send({
      status: httpStatus.OK,
      message: "Successfully saved draft Calibration data"
    });
  }else{
    res.send({
      status: httpStatus.FORBIDDEN,
      message: "Failed to saved draft Calibration data"
    });
  }
});

const getCalibrationDraft = catchAsync(async (req, res) => {
  const DraftData = await CalibrationService.getCalibrationDraft(req.body,req.app.locals.userId);
  if (DraftData[0].length == 0) {
    res.send({
      status:httpStatus.OK,
      message: 'Calibration draft not found',
      data: null,
      details: null
    });
  }else{
    res.send({
      status:httpStatus.OK,
      message: null,
      data: DraftData[0],
      details: null
    });
  }
});

module.exports = {
    createCalibration,
    searchCalibration,
    getCalibrationValue,
    deleteCalibration,
    updateCalibration,
    updateCalibrationStatus,
    createCalibrationDraft,
    getCalibrationDraft
};
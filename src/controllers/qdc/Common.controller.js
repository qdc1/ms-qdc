const httpStatus = require('http-status');
const pick = require('../../utils/pick');
const ApiError = require('../../utils/ApiError');
const catchAsync = require('../../utils/catchAsync');
const { CommonService } = require('../../services');
const CommonAPI = require('../../utils/CommonAPI');
const moment = require('moment');
const axios = require('axios')

const searchLine = catchAsync(async (req, res) => {
  const Line = await CommonService.searchLine();
  if (Line[0].length == 0) {
    res.send({
      status:httpStatus.OK,
      message: 'Line not found',
      data: null,
      details: null
    });
  }else{
    res.send({
      status:httpStatus.OK,
      message: null,
      data: Line[0],
      details: null
    });
  }
});

const systemCommon = catchAsync(async (req, res) => {
  const data = await CommonService.systemCommon(req.body,req.app.locals.userId);
  if (data[0].length == 0) {
    res.send({
      status:httpStatus.OK,
      message: 'Data System not found',
      data: null,
      details: null
    });
  }else{
    res.send({
      status:httpStatus.OK,
      message: null,
      data: data[0],
      details: null
    });
  }
});

const passwordCommon = catchAsync(async (req, res) => {
  const data = await CommonService.passwordCommon(req.body,req.app.locals.userId);
  console.log(data[0][0].STATUS)
  if (data[0].length == 0) {
    res.send({
      status:httpStatus.OK,
      message: 'Incorrect password',
      data: null,
      details: null
    });
  }else{
    if(data[0][0].STATUS == 'TRUE'){
      res.send({
        status:httpStatus.OK,
        message: null,
        data: 'Correct Password',
        details: null
      });
    }else{
      res.send({
        status:httpStatus.NOT_FOUND,
        message: null,
        data: 'Incorrect password',
        details: null
      });
    }
  }
});

module.exports = {
  searchLine,
  systemCommon,
  passwordCommon
};

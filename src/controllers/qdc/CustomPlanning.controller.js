const httpStatus = require('http-status');
const readXlsxFile = require("read-excel-file/node");
const excel = require("exceljs");
const pick = require('../../utils/pick');
const ApiError = require('../../utils/ApiError');
const catchAsync = require('../../utils/catchAsync');
const { CustomPlanningService } = require('../../services');
const CommonAPI = require('../../utils/CommonAPI');
const moment = require('moment');

const searchCustomPlanning = catchAsync(async (req, res) => {
    const filter = req.body.search;
    const options = pick(req.body, ['sortBy','sortOrder', 'limit', 'page']);
    const result = await CustomPlanningService.queryCustomPlanning(filter, options,req.app.locals.userId);
    res.send({
      status:httpStatus.OK,
      message:null,
      details:null,
      data:result.rows,
      totalRecords:result.count,
      totalPages:Math.ceil(result.count/req.body.limit),
      pageNo:req.body.page,
      pageSize:req.body.limit
    });
});

const deleteCustomPlanning = catchAsync(async (req, res) => {
    await CustomPlanningService.deleteCustomPlanning(req.body,req.app.locals.userId);
    res.send({
      status:httpStatus.OK,
      message:"Custom Planning successfuly deleted",
    });
});

const createCustomPlanning = catchAsync(async (req, res) => {
  const CustomPlanning = await CustomPlanningService.createCustomPlanning(req.body,req.app.locals.userId);
  let response = CustomPlanning[0][0].response
  if (response == 200){
    res.send({
      status: httpStatus.OK,
      message: "Successfully inserted data Custom Planning"
    });
  }else{
    res.send({
      status: httpStatus.FORBIDDEN,
      message: "Failed to insert data Custom Planning"
    });
  }
});

const updateCustomPlanning = catchAsync(async (req, res) => {
  await CustomPlanningService.updateCustomPlanning(req.body,req.app.locals.userId);
  res.send({
    status:httpStatus.OK,
    message:"Custom Planning successfuly updated",
  });
});


module.exports = {
    searchCustomPlanning,
    deleteCustomPlanning,
    createCustomPlanning,
    updateCustomPlanning
  };
  
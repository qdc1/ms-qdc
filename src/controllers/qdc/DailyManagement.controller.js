const httpStatus = require('http-status');
const pick = require('../../utils/pick');
const ApiError = require('../../utils/ApiError');
const catchAsync = require('../../utils/catchAsync');
const { DailyManagementService } = require('../../services');
const moment = require('moment');

const getDailyManagement = catchAsync(async (req, res) => {
  const DailyManagement = await DailyManagementService.getDailyManagement(req.body,req.app.locals.userId);
    if (DailyManagement[0].length == 0) {
      res.send({
        status:httpStatus.OK,
        message: 'Daily Management not found',
        data: null,
        details: null
      });
    }else{
      res.send({
        status:httpStatus.OK,
        message: null,
        data: DailyManagement[0],
        details: null
      });
    }
});

const createDailyManagement = catchAsync(async (req, res) => {
  let dataReport = req.body.dataReport
  if(dataReport && dataReport.length > 0){
    req.body.dataReport = JSON.stringify(dataReport)
  }else{
    req.body.dataReport = '[]'
  }
  console.log(req.body)
  const report = await DailyManagementService.createDailyManagement(req.body,req.app.locals.userId);
  let response = report[0][0].response
  if (response == 200){
    res.send({
      status: httpStatus.OK,
      message: "Successfully inserted data Daily Management"
    });
  }else{
    res.send({
      status: httpStatus.FORBIDDEN,
      message: "Failed to insert data Daily Management"
    });
  }
});

const updateDailyManagement = catchAsync(async (req, res) => {
  let dataReport = req.body.dataReport
  if(dataReport && dataReport.length > 0){
    req.body.dataReport = JSON.stringify(dataReport)
  }else{
    req.body.dataReport = '[]'
  }
  const report = await DailyManagementService.updateDailyManagement(req.body,req.app.locals.userId);
  let response = report[0][0].response
  if (response == 200){
    res.send({
      status: httpStatus.OK,
      message: "Successfully update data Daily Management"
    });
  }else{
    res.send({
      status: httpStatus.FORBIDDEN,
      message: "Failed to update data Daily Management"
    });
  }
});

module.exports = {
  getDailyManagement,
  createDailyManagement,
  updateDailyManagement
};

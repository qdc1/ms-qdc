const httpStatus = require('http-status');
const pick = require('../../utils/pick');
const ApiError = require('../../utils/ApiError');
const catchAsync = require('../../utils/catchAsync');
const { MonthlyPlanningService } = require('../../services');
const CommonAPI = require('../../utils/CommonAPI');
const moment = require('moment');

const getReportPlanning = catchAsync(async (req, res) => {

  const lineReport = await MonthlyPlanningService.getMonthlyPlanningLineReport(req.body,req.app.locals.userId)
  let totalLineReport = []
  const ngReport = await MonthlyPlanningService.getMonthlyPlanningNgReport(req.body,req.app.locals.userId);

  if (lineReport[0] != null){
    let dataTotal = {
        R_1M_PLAN: 0,
        R_1M_DO_OK: 0,
        R_1M_DO_NG: 0,
        R_1_3M_PLAN: 0,
        R_1_3M_DO_OK: 0,
        R_1_3M_DO_NG: 0,
        R_1_6M_PLAN: 0,
        R_1_6M_DO_OK: 0,
        R_1_6M_DO_NG: 0,
        R_1Y_PLAN: 0,
        R_1Y_DO_OK: 0,
        R_1Y_DO_NG: 0,
        R_1_2Y_PLAN: 0,
        R_1_2Y_DO_OK: 0,
        R_1_2Y_DO_NG: 0,
        UNPLANNING: 0,
        R_TG_PLAN: 0,
        R_TG_DO_OK: 0,
        R_TG_DO_NG: 0,

    }
    lineReport[0].forEach(e => {
        dataTotal.R_1M_PLAN += e.R_1M_PLAN
        dataTotal.R_1M_DO_OK += e.R_1M_DO_OK
        dataTotal.R_1M_DO_NG += e.R_1M_DO_NG
        dataTotal.R_1_3M_PLAN += e.R_1_3M_PLAN
        dataTotal.R_1_3M_DO_OK += e.R_1_3M_DO_OK
        dataTotal.R_1_3M_DO_NG += e.R_1_3M_DO_NG
        dataTotal.R_1_6M_PLAN += e.R_1_6M_PLAN
        dataTotal.R_1_6M_DO_OK += e.R_1_6M_DO_OK
        dataTotal.R_1_6M_DO_NG += e.R_1_6M_DO_NG
        dataTotal.R_1Y_PLAN += e.R_1Y_PLAN
        dataTotal.R_1Y_DO_OK += e.R_1Y_DO_OK
        dataTotal.R_1Y_DO_NG += e.R_1Y_DO_NG
        dataTotal.R_1_2Y_PLAN += e.R_1_2Y_PLAN
        dataTotal.R_1_2Y_DO_OK += e.R_1_2Y_DO_OK
        dataTotal.R_1_2Y_DO_NG += e.R_1_2Y_DO_NG
        dataTotal.UNPLANNING += 0
        dataTotal.R_TG_PLAN += e.R_TG_PLAN
        dataTotal.R_TG_DO_OK += e.R_TG_DO_OK
        dataTotal.R_TG_DO_NG += e.R_TG_DO_NG
    });
    totalLineReport.push(dataTotal)
  }

  let data = {
    monthReport : req.body.month + '-' + req.body.year,
    lineReport : lineReport[0].length > 0 ? lineReport[0] : [],
    totalLineReport : lineReport[0].length > 0 ? totalLineReport : [],
    ngReport : lineReport[0].length > 0 ? ngReport[0] : [],
  }

  res.send({
    status:httpStatus.OK,
    message: 'Data Report',
    data: data,
    details: null
  });
});

module.exports = {
    getReportPlanning
};

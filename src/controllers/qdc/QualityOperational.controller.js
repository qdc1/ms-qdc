const httpStatus = require('http-status');
const pick = require('../../utils/pick');
const ApiError = require('../../utils/ApiError');
const catchAsync = require('../../utils/catchAsync');
const { QualityOperationalService } = require('../../services');
const CommonAPI = require('../../utils/CommonAPI');
const moment = require('moment');

const searchQtyOperational = catchAsync(async (req, res) => {
  const QtyOperational = await QualityOperationalService.searchQtyOperational();
  if (QtyOperational[0].length == 0) {
    res.send({
      status:httpStatus.OK,
      message: 'Qulaity Operational not found',
      data: null,
      details: null
    });
  }else{
    res.send({
      status:httpStatus.OK,
      message: null,
      data: QtyOperational[0],
      details: null
    });
  }
});

module.exports = {
    searchQtyOperational
};

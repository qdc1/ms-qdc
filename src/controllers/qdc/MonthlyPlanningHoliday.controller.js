const httpStatus = require('http-status');
const pick = require('../../utils/pick');
const ApiError = require('../../utils/ApiError');
const catchAsync = require('../../utils/catchAsync');
const { MonthlyPlanningHolidayService } = require('../../services');
const moment = require('moment');
const axios = require('axios');

const searchMonthlyPlanningHoliday = catchAsync(async (req, res) => {
  console.log(req.body)
  const filter = req.body.search;
  const options = pick(req.body, ['sortBy','sortOrder', 'limit', 'page']);
  const result = await MonthlyPlanningHolidayService.searchMonthlyPlanningHoliday(filter, options,req.app.locals.userId);
  console.log(result.rows)
  res.send({
    status:httpStatus.OK,
    message:null,
    details:null,
    data:result.rows,
    totalRecords:result.count,
    totalPages:Math.ceil(result.count/req.body.limit),
    pageNo:req.body.page,
    pageSize:req.body.limit
  });
});

const createMonthlyPlanningHoliday = catchAsync(async (req, res) => {
  console.log(req.body)
    const dataHoliday = await MonthlyPlanningHolidayService.createMonthlyPlanningHoliday(req.body,req.app.locals.userId);
    let response = dataHoliday[0][0].response
    if (response == 200){
      res.send({
        status: httpStatus.OK,
        message: "Successfully inserted data holiday"
      });
    }else{
      res.send({
        status: httpStatus.FORBIDDEN,
        message: "Failed to insert data holiday"
      });
    }
  });

  const deleteMonthlyPlanningHoliday = catchAsync(async (req, res) => {
    const dataHoliday = await MonthlyPlanningHolidayService.getMonthlyPlanningHoliday(req.body,req.app.locals.userId);
    if (dataHoliday[0].length == 0) {
      res.send({
        status:httpStatus.NOT_FOUND,
        message:"Holiday Planning not found",
      });
    }else{
      await MonthlyPlanningHolidayService.deleteMonthlyPlanningHoliday(req.body,req.app.locals.userId);
      res.send({
        status:httpStatus.OK,
        message:"Holiday Planning successfuly deleted",
      });
    }
  });

  const getMonthlyPlanningHoliday = catchAsync(async (req, res) => {
    const dataHoliday = await MonthlyPlanningHolidayService.getMonthlyPlanningHoliday(req.body,req.app.locals.userId);
    if (dataHoliday[0].length == 0) {
      res.send({
        status:httpStatus.OK,
        message: 'Holiday Planning not found',
        data: null,
        details: null
      });
    }else{
      res.send({
        status:httpStatus.OK,
        message: null,
        data: dataHoliday[0],
        details: null
      });
    }
  });

  const updateMonthlyPlanningHoliday = catchAsync(async (req, res) => {
    const dataHoliday = await MonthlyPlanningHolidayService.getMonthlyPlanningHoliday(req.body,req.app.locals.userId);
    if (dataHoliday[0].length == 0) {
      res.send({
        status:httpStatus.NOT_FOUND,
        message:"Holiday Planning not found",
      });
    }else{
      await MonthlyPlanningHolidayService.updateMonthlyPlanningHoliday(req.body,req.app.locals.userId);
      res.send({
        status:httpStatus.OK,
        message:"Holiday Planning successfuly updated",
      });
    }
  });

module.exports = {
  searchMonthlyPlanningHoliday,
  createMonthlyPlanningHoliday,
  getMonthlyPlanningHoliday,
  updateMonthlyPlanningHoliday,
  deleteMonthlyPlanningHoliday
};

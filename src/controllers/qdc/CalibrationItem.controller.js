const httpStatus = require('http-status');
const pick = require('../../utils/pick');
const ApiError = require('../../utils/ApiError');
const catchAsync = require('../../utils/catchAsync');
const { CalibrationItemService } = require('../../services');
const CommonAPI = require('../../utils/CommonAPI');
const moment = require('moment');

const createCalItem = catchAsync(async (req, res) => {
  let calItems = req.body.cal_items
  if(calItems && calItems.length > 0){
    req.body.cal_items = JSON.stringify(calItems)
    const CalItem = await CalibrationItemService.createCalItem(req.body,req.app.locals.userId);
    let response = CalItem[0][0].response
    if (response == 200){
      res.send({
        status: httpStatus.OK,
        message: "Successfully inserted data Calibration Item"
      });
    }else{
      res.send({
        status: httpStatus.FORBIDDEN,
        message: "Failed to insert data Calibration Item"
      });
    }
  }else{
    res.send({
      status: httpStatus.NOT_FOUND,
      message: "Calibration Item Not Found"
    });
  }
  
});

const searchCalItem = catchAsync(async (req, res) => {
  const filter = req.body.search;
  const options = pick(req.body, ['sortBy','sortOrder', 'limit', 'page']);
  const result = await CalibrationItemService.queryCalItems(filter, options,req.app.locals.userId);
  res.send({
    status:httpStatus.OK,
    message:null,
    details:null,
    data:result.rows,
    totalRecords:result.count,
    totalPages:Math.ceil(result.count/req.body.limit),
    pageNo:req.body.page,
    pageSize:req.body.limit
  });
});

const deleteCalItem = catchAsync(async (req, res) => {
    await CalibrationItemService.deleteCalItem(req.body,req.app.locals.userId);
    res.send({
      status:httpStatus.OK,
      message:"Calibration Item successfuly deleted",
    });
});

const getCalItem = catchAsync(async (req, res) => {
  const CalItem = await CalibrationItemService.getCalItem(req.body,req.app.locals.userId);
  if (CalItem[0].length == 0) {
    res.send({
      status:httpStatus.OK,
      message: 'Calibration Item not found',
      data: null,
      details: null
    });
  }else{
    res.send({
      status:httpStatus.OK,
      message: null,
      data: CalItem[0],
      details: null
    });
  }
});

const updateCalItem = catchAsync(async (req, res) => {

    await CalibrationItemService.updateCalItem(req.body,req.app.locals.userId);
    res.send({
      status:httpStatus.OK,
      message:"Calibration Item successfuly updated",
    });
});

module.exports = {
  searchCalItem,
  getCalItem,
  deleteCalItem,
  createCalItem,
  updateCalItem
};

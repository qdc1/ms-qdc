const httpStatus = require('http-status');
const pick = require('../../utils/pick');
const ApiError = require('../../utils/ApiError');
const catchAsync = require('../../utils/catchAsync');
const { MessageService } = require('../../services');
const moment = require('moment');
const axios = require('axios');

const createMessage = catchAsync(async (req, res) => {
    const dataMessage = await MessageService.createMessage(req.body,req.app.locals.userId);
    let response = dataMessage[0][0].response
    if (response == 200){
      res.send({
        status: httpStatus.OK,
        message: "Successfully inserted data message"
      });
    }else{
      res.send({
        status: httpStatus.FORBIDDEN,
        message: "Failed to insert data message"
      });
    }
  });

  const getMessage = catchAsync(async (req, res) => {
    const dataMessage = await MessageService.getMessage(req.body,req.app.locals.userId);
    if (dataMessage[0].length == 0) {
      res.send({
        status:httpStatus.OK,
        message: 'Message not found',
        data: null,
        details: null
      });
    }else{
      res.send({
        status:httpStatus.OK,
        message: null,
        data: dataMessage[0],
        details: null
      });
    }
  });


module.exports = {
  createMessage,
  getMessage
};

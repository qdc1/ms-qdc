const Joi = require('joi');


const searchCalItem = {
    body: Joi.object()
      .keys({
        search:{
          keyword: Joi.string().max(255).allow(null,''),
          idGauge: Joi.number().integer().allow(null,''),
        },
        page:Joi.number().integer().allow(null,''),
        limit:Joi.number().integer().allow(null,''),
        sortBy:Joi.string().allow(null,''),
        sortOrder:Joi.string().allow(null,'')
    }),
};

const deleteCalItem = {
  body: Joi.object()
    .keys({
      gd_id: Joi.number().integer().required()
    }),
};

const getCalItem = {
  body: Joi.object()
    .keys({
      gd_id: Joi.number().integer().required()
    }),
};


const createCalItem = {
  body: Joi.object().keys({
    cal_items: Joi.array().allow(null,''),
    // gd_gauge_id: Joi.number().integer().required(),
    // gd_gauge_item: Joi.string().max(200).required(),
    // gd_gauge_item_cal: Joi.string().max(200).required(),
    // gd_gauge_item_cal_min: Joi.number().required(),
    // gd_gauge_item_cal_max: Joi.number().required()
  }),
};

const updateCalItem = {
  body: Joi.object().keys({
    gd_id: Joi.number().integer().required(),
    gd_gauge_id: Joi.number().integer().required(),
    gd_gauge_item: Joi.string().max(200).required(),
    gd_gauge_item_cal: Joi.string().max(200).required(),
    gd_gauge_item_cal_min: Joi.number().required(),
    gd_gauge_item_cal_max: Joi.number().required()
  }),
};

module.exports = {
  searchCalItem,
  getCalItem,
  deleteCalItem,
  createCalItem,
  updateCalItem
};

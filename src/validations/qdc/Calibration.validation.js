const Joi = require('joi');


const createCalibration = {
  body: Joi.object().keys({
    calibration_gauge_id: Joi.number().integer().required(),
    calibration_date: Joi.string().max(50).required(),
    calibration_temperature: Joi.number().required(),
    calibration_humidity: Joi.number().required(),
    calibration_created_by: Joi.string().max(100).required(),
    calibration_upload_evidence: Joi.string().allow(null,''),
    calibration_judgement: Joi.string().max(10).required(),
    calibration_value: Joi.array().allow(null,''),
  }),
};

const updateCalibration = {
  body: Joi.object().keys({
    calibration_id: Joi.number().integer().required(),
    calibration_remark: Joi.string().max(200).allow(null,'')
  }),
};

const updateCalibrationStatus = {
  body: Joi.object().keys({
    calibration_id: Joi.number().integer().required(),
    calibration_status_ng: Joi.number().integer().required()
  }),
};

const searchCalibration = {
  body: Joi.object()
    .keys({
      search:{
        keyword: Joi.string().max(255).allow(null,''),
        idGauge: Joi.number().integer().allow(null,''),
      },
      page:Joi.number().integer().allow(null,''),
      limit:Joi.number().integer().allow(null,''),
      sortBy:Joi.string().allow(null,''),
      sortOrder:Joi.string().allow(null,'')
  }),
};

const getCalibrationValue = {
  body: Joi.object()
    .keys({
      cd_cal_id: Joi.number().integer().required()
    }),
};

const deleteCalibration = {
  body: Joi.object()
    .keys({
      cal_id: Joi.number().integer().required()
    }),
};

const createCalibrationDraft = {
  body: Joi.object().keys({
    draftGaugeId: Joi.number().integer().required(),
    draftData: Joi.string().allow(null,''),
  }),
};

const getCalibrationDraft = {
  body: Joi.object().keys({
    gauge_id: Joi.number().integer().required()
  }),
};

module.exports = {
    createCalibration,
    searchCalibration,
    getCalibrationValue,
    deleteCalibration,
    updateCalibration,
    updateCalibrationStatus,
    createCalibrationDraft,
    getCalibrationDraft
};

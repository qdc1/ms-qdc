const Joi = require('joi');

const getDailyManagement = {
  body: Joi.object()
    .keys({
      month: Joi.string().max(20).required(),
      year: Joi.string().max(20).required(),
    }),
};

const createDailyManagement = {
  body: Joi.object().keys({
    month: Joi.string().max(20).required(),
    year: Joi.string().max(20).required(),
    dataReport: Joi.array().allow(null,''),
  }),
};

const updateDailyManagement = {
  body: Joi.object().keys({
    dataReport: Joi.array().allow(null,''),
  }),
};


module.exports = {
  getDailyManagement,
  createDailyManagement,
  updateDailyManagement
};

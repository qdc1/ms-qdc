const Joi = require('joi');

const getMessage = {
    body: Joi.object()
      .keys({
        cm_cal_id: Joi.number().integer().required()
      }),
};

const createMessage = {
    body: Joi.object().keys({
        cm_cal_id: Joi.number().integer().required(),
        cm_message: Joi.string().required(),
  }),
}


module.exports = {
  createMessage,
  getMessage
};

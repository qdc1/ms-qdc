const Joi = require('joi');


const searchMonthlyPlanningHoliday = {
    body: Joi.object()
      .keys({
        search:{
          keyword: Joi.string().max(255).allow(null,''),
        },
        page:Joi.number().integer().allow(null,''),
        limit:Joi.number().integer().allow(null,''),
        sortBy:Joi.string().allow(null,''),
        sortOrder:Joi.string().allow(null,'')
    }),
};

const deleteMonthlyPlanningHoliday = {
  body: Joi.object()
    .keys({
      GphId: Joi.number().integer().required()
    }),
};

const getMonthlyPlanningHoliday = {
    body: Joi.object()
      .keys({
        GphId: Joi.number().integer().required()
      }),
};

const createMonthlyPlanningHoliday = {
    body: Joi.object().keys({
        GphDescription: Joi.string().max(100).required(),
        GphDate: Joi.string().max(25).required(),
        GphIsNationalHoliday: Joi.string().max(5).required(),
        GphIsWork: Joi.string().max(5).required(),
  }),
}

const updateMonthlyPlanningHoliday = {
    body: Joi.object().keys({
        GphId: Joi.number().integer().required(),
        GphDescription: Joi.string().max(100).required(),
        GphDate: Joi.string().max(25).required(),
        GphIsNationalHoliday: Joi.string().max(5).required(),
        GphIsWork: Joi.string().max(5).required(),
  }),
}


module.exports = {
  createMonthlyPlanningHoliday,
  searchMonthlyPlanningHoliday,
  deleteMonthlyPlanningHoliday,
  updateMonthlyPlanningHoliday,
  getMonthlyPlanningHoliday
};

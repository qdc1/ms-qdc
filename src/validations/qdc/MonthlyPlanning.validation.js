const Joi = require('joi');


const searchMonthlyPlanning = {};

const createMonthlyPlanning = {
  body: Joi.object().keys({
    month: Joi.string().max(50).required(),
    year: Joi.string().max(50).required(),
  }),
};

const getMonthlyPlanning = {
  body: Joi.object().keys({
    month: Joi.string().max(50).required(),
    year: Joi.string().max(50).required(),
  }),
};

const getMonthlyPlanningLineReport = {
  body: Joi.object().keys({
    month: Joi.string().max(50).required(),
    year: Joi.string().max(50).required(),
  }),
};


module.exports = {
  searchMonthlyPlanning,
  createMonthlyPlanning,
  getMonthlyPlanning,
  getMonthlyPlanningLineReport
};

const Joi = require('joi');


const searchCustomPlanning = {
    body: Joi.object()
      .keys({
        search:{
          keyword: Joi.string().max(255).allow(null,''),
          startDate: Joi.string().max(40).allow(null,''),
          endDate: Joi.string().max(40).allow(null,''),
        },
        page:Joi.number().integer().allow(null,''),
        limit:Joi.number().integer().allow(null,''),
        sortBy:Joi.string().allow(null,''),
        sortOrder:Joi.string().allow(null,'')
    }),
};

const deleteCustomPlanning = {
  body: Joi.object()
    .keys({
      gpd_id: Joi.number().integer().required()
    }),
};

const createCustomPlanning = {
  body: Joi.object().keys({
    gpd_date: Joi.string().max(25).required(),
    gpd_gauge_id: Joi.number().integer().required(),
  }),
};

const updateCustomPlanning = {
  body: Joi.object().keys({
    gpd_id: Joi.number().integer().required(),
    gpd_date: Joi.string().max(25).required(),
  }),
};

module.exports = {
  searchCustomPlanning,
  deleteCustomPlanning,
  createCustomPlanning,
  updateCustomPlanning
};

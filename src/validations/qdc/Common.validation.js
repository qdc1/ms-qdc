const Joi = require('joi');

const passwordCommon = {
  body: Joi.object()
    .keys({
        password: Joi.string().max(200).allow(null,''),
    }),
};

module.exports = {
  passwordCommon
};

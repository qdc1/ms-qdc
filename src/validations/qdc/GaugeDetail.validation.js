const Joi = require('joi');

const getGaugeDetail = {
  body: Joi.object()
    .keys({
      id: Joi.number().integer().required(),
      columnBy: Joi.string().max(50).required(),
    }),
};

const downloadGaugeDetail = {
  body: Joi.object()
    .keys({
      id: Joi.number().integer().required(),
      chartBase64: Joi.string().allow(null,''),
    }),
};


module.exports = {
  getGaugeDetail,
  downloadGaugeDetail
};

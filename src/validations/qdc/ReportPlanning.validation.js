const Joi = require('joi');

const getReportPlanning = {
    body: Joi.object()
      .keys({
        month: Joi.string().max(20).required(),
        year: Joi.string().max(20).required(),
      }),
  };

module.exports = {
    getReportPlanning
};

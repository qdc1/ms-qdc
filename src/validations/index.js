module.exports.GaugeValidation = require('./qdc/Gauge.validation');
module.exports.GaugeDetailValidation = require('./qdc/GaugeDetail.validation');
module.exports.QualityOperationalValidation = require('./qdc/QualityOperational.validation');
module.exports.CalibrationValidation = require('./qdc/Calibration.validation');
module.exports.CalibrationItemValidation = require('./qdc/CalibrationItem.validation');
module.exports.MonthlyPlanningValidation = require('./qdc/MonthlyPlanning.validation');
module.exports.MonthlyPlanningHolidayValidation = require('./qdc/MonthlyPlanningHoliday.validation');
module.exports.DailyManagementValidation = require('./qdc/DailyManagement.validation');
module.exports.ReportPlanningValidation = require('./qdc/ReportPlanning.validation');
module.exports.CustomPlanningValidation = require('./qdc/CustomPlanning.validation');
module.exports.MessageValidation = require('./qdc/Message.validation');
/*define other in here*/

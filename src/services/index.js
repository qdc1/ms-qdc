const Sequelize = require('sequelize');
const { qdc_db, platform_db } = require('../config/config');

const qdc_sequelize = new Sequelize(qdc_db.database, qdc_db.username, qdc_db.password, qdc_db);

//https://github.com/tagomoris/presto-client-node
// const presto = require('presto-client');
// const prestoClient = new presto.Client(
//     {   user: platform_db.username,host :platform_db.host,port:platform_db.port,
//         basic_auth: {user: platform_db.username, password: platform_db.password} });
//https://github.com/tagomoris/presto-client-node
// const presto = require('presto-client');
// // const platform_presto = null;
// const platform_presto = new presto.Client(
//     {   user: platform_db.username,host :platform_db.host,port:platform_db.port,
//         basic_auth: {user: platform_db.username, password: platform_db.password} });

module.exports.GaugeService = require('./qdc/Gauge.service')(qdc_sequelize,Sequelize);
module.exports.GaugeDetailService = require('./qdc/GaugeDetail.service')(qdc_sequelize,Sequelize);
module.exports.QualityOperationalService = require('./qdc/QualityOperational.service')(qdc_sequelize,Sequelize);
module.exports.CalibrationService = require('./qdc/Calibration.service')(qdc_sequelize,Sequelize);
module.exports.CommonService = require('./qdc/Common.service')(qdc_sequelize,Sequelize);
module.exports.CalibrationItemService = require('./qdc/CalibrationItem.service')(qdc_sequelize,Sequelize);
module.exports.MonthlyPlanningService = require('./qdc/MonthlyPlanning.service')(qdc_sequelize,Sequelize);
module.exports.MonthlyPlanningHolidayService = require('./qdc/MonthlyPlanningHoliday.service')(qdc_sequelize,Sequelize);
module.exports.DailyManagementService = require('./qdc/DailyManagement.service')(qdc_sequelize,Sequelize);
module.exports.CustomPlanningService = require('./qdc/CustomPlanning.service')(qdc_sequelize,Sequelize);
module.exports.MessageService = require('./qdc/Message.service')(qdc_sequelize,Sequelize);

/*define other in here*/
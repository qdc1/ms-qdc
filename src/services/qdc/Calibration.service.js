const httpStatus = require('http-status');
const { Gauge } = require('../../models');
const ApiError = require('../../utils/ApiError');

module.exports = (sequelize, Sequelize) => {
  class CalibrationService {

      /**
     * Create a Calibration
     * @param {Object} Calibration
     * @param {String} userId
     * @returns {Promise<Calibration>}
     */
    static createCalibration = async (Calibration,userId) => {
      return new Promise(async (resolve, reject) => {
      await sequelize.query(
        'qdc_calibration_insert_data'
        + ' @calibrationGaugeId = :calibration_gauge_id,'
        + ' @calibrationDate = :calibration_date,'
        + ' @calibrationTemperature = :calibration_temperature,'
        + ' @calibrationHumidity = :calibration_humidity,'
        + ' @calibrationCreatedBy = :calibration_created_by,'
        + ' @calibrationUploadEvidence = :calibration_upload_evidence,'
        + ' @calibrationJudgement = :calibration_judgement,'
        + ' @calibrationValue = :calibration_value,'

        + " @createdBy = '"+userId+"'"
        ,{
            replacements: Calibration
        },
        Sequelize.QueryTypes.INSERT
        )
        .then(result => {
          resolve(result);
        })
        .catch(error => {
          reject(error);
        });
      })
    };

    /**
     * Update a Calibration
     * @param {Object} Calibration
     * @param {String} userId
     * @returns {Promise<Calibration>}
     */
    static updateCalibration = async (Calibration,userId) => {
      return new Promise(async (resolve, reject) => {
      await sequelize.query(
        'qdc_calibration_update_data'
        + ' @calibrationId = :calibration_id,'
        + ' @calibrationRemark = :calibration_remark,'
        
        + " @changedBy = '"+userId+"'"
        ,{
            replacements: Calibration
        },
        Sequelize.QueryTypes.UPDATE
        )
        .then(result => {
          resolve(result);
        })
        .catch(error => {
          reject(error);
        });
      })
    };

    static paginateCal = async (filter, options, userId) => {
      return new Promise(async(resolve, reject) => {
        let params = JSON.parse(JSON.stringify(filter));
        params.page = options.page;
        params.limit = options.limit;
        params.sortBy = options.sortBy;
        params.sortOrder = options.sortOrder;

        await sequelize.query(
            "qdc_calibration_search_data"
                      + " @keyword = :keyword,"
                      + " @idGauge = :idGauge,"
                      + " @searchBy = '" + userId + "',"
                      + " @page = :page,"
                      + " @rowPerPage = :limit,"
                      + " @orderBy = :sortBy,"
                      + " @sortOrder = :sortOrder"
          ,{
              replacements: params
          }
          ,Sequelize.QueryTypes.SELECT
          )
          .then(result => {
            if(result.length == 0){
              resolve({count:0, rows:[]});
            }else
            if(result[0][0]['FNAME'] && result[0][0]['FNAME']=='ROW_COUNT'){
              let rowCount = result[0].splice(0,1);
              let count = rowCount[0]['FVAL'];
              let rows = result[0];
              resolve({count:count, rows: rows});
            }else{
              resolve(result);
            }
          })
          .catch(error => {
            reject(error);
          });
      })
    };

    /**
     * Query for 
     * @param {Object} filter - Sequelize filter
     * @param {Object} options - Query options
     * @param {string} [options.sortOrder] - Sort option in the format: ['field1'],['field2']
     * @param {string} [options.sortBy] - Sort option in the format: ['ASC'],['DESC']
     * @param {number} [options.limit] - Maximum number of results per page (default = 10)
     * @param {number} [options.page] - Current page (default = 1)
     * @param {String} userId
     * @returns {Promise<QueryResult>}
     */
    static queryCal = async (filter, options, userId) => {
      const Data = await this.paginateCal(filter, options, userId);
      return Data;
    };

        /**
     * Get Calibration 
     * @param {Object} Calibration
     * @param {String} userId
     * @returns {Promise<Calibration>}
     */
        static getCalibrationValue = async (Calibration,userId) => {
          return new Promise(async(resolve, reject) => {
            await sequelize.query(
              "qdc_calibration_value_get_data"
                  + ' @CdCalId = :cd_cal_id,'
    
                  + " @searchBy = '"+userId+"'"
              ,{
                  replacements: Calibration
              }
              ,Sequelize.QueryTypes.SELECT
              )
              .then(result => {
                resolve(result);
              })
              .catch(error => {
                reject(error);
              });
          })
    };

    /**
     * Delete Calibration
     * @param {Object} Calibration
     * @param {String} userId
     * @returns {Promise<Calibration>}
     */
    static deleteCalibration = async (Calibration,userId) => {
      await sequelize.query(
        'qdc_calibration_delete_data'
        + ' @calId = :cal_id,'

        + " @changedBy = '"+userId+"'"
        ,{
            replacements: Calibration
        },
        Sequelize.QueryTypes.UPDATE
        )
        .then(result => {
          return result;
        })
        .catch(error => {
          throw(error);
        });
    };

       /**
     * Get Calibration 
     * @param {Object} Calibration
     * @param {String} userId
     * @returns {Promise<Calibration>}
     */
       static getAllCalibrationAllValue = async (Calibration,userId) => {
        return new Promise(async(resolve, reject) => {
          await sequelize.query(
            "qdc_calibration_value_get_all_data"
                + ' @GaugeId = :gauge_id,'
  
                + " @searchBy = '"+userId+"'"
            ,{
                replacements: Calibration
            }
            ,Sequelize.QueryTypes.SELECT
            )
            .then(result => {
              resolve(result);
            })
            .catch(error => {
              reject(error);
            });
        })
    };

     /**
     * Update Calibration Status
     */
     static updateCalibrationStatus = async (Calibration, userId) => {
        await sequelize.query(
          'qdc_calibration_update_status_data'
            + ' @calibrationId = :calibration_id,'
            + ' @calibrationJudgement = :calibration_status_ng,'
    
            + " @changedBy = '"+userId+"'"
            ,{
                replacements: Calibration
            },
            Sequelize.QueryTypes.UPDATE
          )
          .then(result => {
            return result;
          })
          .catch(error => {
            throw(error);
          });
    };

      /**
     * Create a Calibration Draft
     * @param {Object} Calibration
     * @param {String} userId
     * @returns {Promise<Calibration>}
     */
      static createCalibrationDraft = async (Calibration,userId) => {
        return new Promise(async (resolve, reject) => {
        await sequelize.query(
          'qdc_calibration_insert_draft_data'
          + ' @draftGaugeId = :draftGaugeId,'
          + ' @draftData = :draftData,'
  
          + " @createdBy = '"+userId+"'"
          ,{
              replacements: Calibration
          },
          Sequelize.QueryTypes.INSERT
          )
          .then(result => {
            resolve(result);
          })
          .catch(error => {
            reject(error);
          });
        })
      };

          /**
     * Get Calibration draft
     * @param {Object} Calibration
     * @param {String} userId
     * @returns {Promise<Calibration>}
     */
          static getCalibrationDraft = async (Calibration,userId) => {
            return new Promise(async(resolve, reject) => {
              await sequelize.query(
                "qdc_calibration_get_draft_data"
                    + ' @gaugeId = :gauge_id,'
      
                    + " @searchBy = '"+userId+"'"
                ,{
                    replacements: Calibration
                }
                ,Sequelize.QueryTypes.SELECT
                )
                .then(result => {
                  resolve(result);
                })
                .catch(error => {
                  reject(error);
                });
            })
      };
  

  }
  return CalibrationService;
};

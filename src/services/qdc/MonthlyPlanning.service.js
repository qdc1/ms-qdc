const httpStatus = require('http-status');
const { MonthlyPlanning } = require('../../models');
const ApiError = require('../../utils/ApiError');

module.exports = (sequelize, Sequelize) => {
  class MonthlyPlanningService {
    /**
     * Get Gauge
     * @param {Object} MonthlyPlanning
     * @param {String} userId
     * @returns {Promise<Gauge>}
     */
    static searchMonthlyPlanning = async (MonthlyPlanning, userId) => {
        return new Promise(async(resolve, reject) => {
          await sequelize.query(
            "qdc_monthly_planning_search_data"
                + ' @isDetail = :isDetail,'
                + ' @date = :date,'
                + " @searchBy = '"+userId+"'"
            ,{
                replacements: MonthlyPlanning
            }
            ,Sequelize.QueryTypes.SELECT
            )
            .then(result => {
              resolve(result);
            })
            .catch(error => {
              reject(error);
            });
        })
      };

      /**
     * Get MonthlyPlanning
     * @param {Object} MonthlyPlanning
     * @param {String} userId
     * @returns {Promise<MonthlyPlanning>}
     */
    static getMonthlyPlanning = async (MonthlyPlanning, userId) => {
      return new Promise(async(resolve, reject) => {
        await sequelize.query(
          "qdc_monthly_planning_check_data"
              + ' @month = :month,'
              + ' @year = :year,'

              + " @searchBy = '"+userId+"'"
          ,{
              replacements: MonthlyPlanning
          }
          ,Sequelize.QueryTypes.SELECT
          )
          .then(result => {
            resolve(result);
          })
          .catch(error => {
            reject(error);
          });
      })
    };

      /**
     * Get exp gauge
     * @param {Object} MonthlyPlanning
     * @param {String} userId
     * @returns {Promise<Gauge>}
     */
    static getGaugeExpLastDay = async (MonthlyPlanning, userId) => {
      return new Promise(async(resolve, reject) => {
        await sequelize.query(
          "qdc_gauge_expired_lastday_get_data"
              + ' @startDate = :startDate,'
              + ' @endDate = :endDate,'
              + " @searchBy = '"+userId+"'"
          ,{
              replacements: MonthlyPlanning
          }
          ,Sequelize.QueryTypes.SELECT
          )
          .then(result => {
            resolve(result);
          })
          .catch(error => {
            reject(error);
          });
      })
    };

      /**
     * Get exp gauge
     * @param {Object} MonthlyPlanning
     * @param {String} userId
     * @returns {Promise<Gauge>}
     */
      static getGaugeExpMonth = async (MonthlyPlanning, userId) => {
        return new Promise(async(resolve, reject) => {
          await sequelize.query(
            "qdc_gauge_expired_monthly_get_data"
                + ' @startDate = :startDate,'
                + ' @endDate = :endDate,'
                + " @searchBy = '"+userId+"'"
            ,{
                replacements: MonthlyPlanning
            }
            ,Sequelize.QueryTypes.SELECT
            )
            .then(result => {
              resolve(result);
            })
            .catch(error => {
              reject(error);
            });
        })
      };

    /**
     * Create a Generate
     * @param {Object} Generate
     * @param {String} userId
     * @returns {Promise<Generate>}
     */
    static createMonthlyPlanning = async (Generate,userId) => {
      return new Promise(async (resolve, reject) => {
      await sequelize.query(
        'qdc_monthly_planning_generate_insert_data'
        + ' @GpDate = :GpDate,'
        + ' @GpDescription = :GpDescription,'
        + ' @DataGauges = :DataGauges,'
        
        + " @createdBy = '"+userId+"'"
        ,{
            replacements: Generate
        },
        Sequelize.QueryTypes.INSERT
        )
        .then(result => {
          resolve(result);
        })
        .catch(error => {
          reject(error);
        });
      })
    };

      /**
     * Get MonthlyPlanningLineReport
     * @param {Object} MonthlyPlanningLineReport
     * @param {String} userId
     * @returns {Promise<MonthlyPlanningLineReport>}
     */
      static getMonthlyPlanningLineReport = async (MonthlyPlanningLineReport, userId) => {
        return new Promise(async(resolve, reject) => {
          await sequelize.query(
            "qdc_monthly_planning_report_line_data"
                + ' @month = :month,'
                + ' @year = :year,'
  
                + " @searchBy = '"+userId+"'"
            ,{
                replacements: MonthlyPlanningLineReport
            }
            ,Sequelize.QueryTypes.SELECT
            )
            .then(result => {
              resolve(result);
            })
            .catch(error => {
              reject(error);
            });
        })
      };

       /**
     * Get MonthlyPlanningNgReport
     * @param {Object} MonthlyPlanningNgReport
     * @param {String} userId
     * @returns {Promise<MonthlyPlanningNgReport>}
     */
       static getMonthlyPlanningNgReport = async (MonthlyPlanningNgReport, userId) => {
        return new Promise(async(resolve, reject) => {
          await sequelize.query(
            "qdc_monthly_planning_report_ng_data"
                + ' @month = :month,'
                + ' @year = :year,'
  
                + " @searchBy = '"+userId+"'"
            ,{
                replacements: MonthlyPlanningNgReport
            }
            ,Sequelize.QueryTypes.SELECT
            )
            .then(result => {
              resolve(result);
            })
            .catch(error => {
              reject(error);
            });
        })
      };

  }
  return MonthlyPlanningService;
};

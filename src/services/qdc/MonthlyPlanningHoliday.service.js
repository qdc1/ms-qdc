const httpStatus = require('http-status');
const { MonthlyPlanningHoliday } = require('../../models');
const ApiError = require('../../utils/ApiError');

module.exports = (sequelize, Sequelize) => {
  class MonthlyPlanningHolidayService {

      /**
     * Create a MonthlyPlanningHoliday
     * @param {Object} MonthlyPlanningHoliday
     * @param {String} userId
     * @returns {Promise<MonthlyPlanningHoliday>}
     */
    static createMonthlyPlanningHoliday = async (MonthlyPlanningHoliday,userId) => {
      return new Promise(async (resolve, reject) => {
      await sequelize.query(
        'qdc_monthly_planning_holiday_insert_data'
        + ' @GphDescription = :GphDescription,'
        + ' @GphDate = :GphDate,'
        + ' @GphIsNationalHoliday = :GphIsNationalHoliday,'
        + ' @GphIsWork = :GphIsWork,'
        
        + " @createdBy = '"+userId+"'"
        ,{
            replacements: MonthlyPlanningHoliday
        },
        Sequelize.QueryTypes.INSERT
        )
        .then(result => {
          resolve(result);
        })
        .catch(error => {
          reject(error);
        });
      })
    };

    /**
     * Create a MonthlyPlanningHolidayApiPublic
     * @param {Object} MonthlyPlanningHolidayApiPublic
     * @param {String} userId
     * @returns {Promise<MonthlyPlanningHolidayApiPublic>}
     */
    static createMonthlyPlanningHolidayApiPublic = async (MonthlyPlanningHolidayApiPublic,userId) => {
        return new Promise(async (resolve, reject) => {
        await sequelize.query(
          'qdc_monthly_planning_holiday_apipublic_insert_data'
          + ' @jsonData = :jsonData,'
          
          + " @createdBy = '"+userId+"'"
          ,{
              replacements: MonthlyPlanningHolidayApiPublic
          },
          Sequelize.QueryTypes.INSERT
          )
          .then(result => {
            resolve(result);
          })
          .catch(error => {
            reject(error);
          });
        })
      };


    /**
     * Update a MonthlyPlanningHoliday
     * @param {Object} MonthlyPlanningHoliday
     * @param {String} userId
     * @returns {Promise<MonthlyPlanningHoliday>}
     */
     static updateMonthlyPlanningHoliday = async (MonthlyPlanningHoliday,userId) => {
      return new Promise(async (resolve, reject) => {
      await sequelize.query(
        'qdc_monthly_planning_holiday_update_data'
        + ' @GphId = :GphId,'
        + ' @GphDescription = :GphDescription,'
        + ' @GphDate = :GphDate,'
        + ' @GphIsNationalHoliday = :GphIsNationalHoliday,'
        + ' @GphIsWork = :GphIsWork,'
        
        + " @changedBy = '"+userId+"'"
        ,{
            replacements: MonthlyPlanningHoliday
        },
        Sequelize.QueryTypes.UPDATE
        )
        .then(result => {
          resolve(result);
        })
        .catch(error => {
          reject(error);
        });
      })
    };

    static paginateData = async (filter, options, userId) => {
      return new Promise(async(resolve, reject) => {
        let params = JSON.parse(JSON.stringify(filter));
        params.page = options.page;
        params.limit = options.limit;
        params.sortBy = options.sortBy;
        params.sortOrder = options.sortOrder;

        await sequelize.query(
            "qdc_monthly_planning_holiday_search_data"
                      + " @keyword = :keyword,"
                      + " @searchBy = '" + userId + "',"
                      + " @page = :page,"
                      + " @rowPerPage = :limit,"
                      + " @orderBy = :sortBy,"
                      + " @sortOrder = :sortOrder"
          ,{
              replacements: params
          }
          ,Sequelize.QueryTypes.SELECT
          )
          .then(result => {
            if(result.length == 0){
              resolve({count:0, rows:[]});
            }else
            if(result[0][0]['FNAME'] && result[0][0]['FNAME']=='ROW_COUNT'){
              let rowCount = result[0].splice(0,1);
              let count = rowCount[0]['FVAL'];
              let rows = result[0];
              resolve({count:count, rows: rows});
            }else{
              resolve(result);
            }
          })
          .catch(error => {
            reject(error);
          });
      })
    };

    /**
     * Query for 
     * @param {Object} filter - Sequelize filter
     * @param {Object} options - Query options
     * @param {string} [options.sortOrder] - Sort option in the format: ['field1'],['field2']
     * @param {string} [options.sortBy] - Sort option in the format: ['ASC'],['DESC']
     * @param {number} [options.limit] - Maximum number of results per page (default = 10)
     * @param {number} [options.page] - Current page (default = 1)
     * @param {String} userId
     * @returns {Promise<QueryResult>}
     */
    static searchMonthlyPlanningHoliday = async (filter, options, userId) => {
      const Data = await this.paginateData(filter, options, userId);
      return Data;
    };

    /**
     * Delete deleteMonthlyPlanningHoliday
     * @param {Object} MonthlyPlanningHoliday
     * @param {String} userId
     * @returns {Promise<deleteMonthlyPlanningHoliday>}
     */
    static deleteMonthlyPlanningHoliday = async (MonthlyPlanningHoliday,userId) => {
      await sequelize.query(
        'qdc_monthly_planning_holiday_delete_data'
        + ' @GphId = :GphId,'

        + " @changedBy = '"+userId+"'"
        ,{
            replacements: MonthlyPlanningHoliday
        },
        Sequelize.QueryTypes.UPDATE
        )
        .then(result => {
          return result;
        })
        .catch(error => {
          throw(error);
        });
    };

    /**
     * Get MonthlyPlanningHoliday
     * @param {Object} MonthlyPlanningHoliday
     * @param {String} userId
     * @returns {Promise<MonthlyPlanningHoliday>}
     */
    static getMonthlyPlanningHoliday = async (MonthlyPlanningHoliday,userId) => {
      return new Promise(async(resolve, reject) => {
        await sequelize.query(
          "qdc_monthly_planning_holiday_get_data"
              + ' @GphId = :GphId,'

              + " @searchBy = '"+userId+"'"
          ,{
              replacements: MonthlyPlanningHoliday
          }
          ,Sequelize.QueryTypes.SELECT
          )
          .then(result => {
            resolve(result);
          })
          .catch(error => {
            reject(error);
          });
      })
    };


  }
  return MonthlyPlanningHolidayService;
};

const httpStatus = require('http-status');
const { Gauge } = require('../../models');
const ApiError = require('../../utils/ApiError');

module.exports = (sequelize, Sequelize) => {
  class QualityOperationalService {

    /**
     * Get Quality Operational
     */
    static searchQtyOperational = async () => {
      return new Promise(async(resolve, reject) => {
        await sequelize.query(
          "qdc_quality_operational_get_data"
          ,Sequelize.QueryTypes.SELECT
          )
          .then(result => {
            resolve(result);
          })
          .catch(error => {
            reject(error);
          });
      })
    };

  }
  return QualityOperationalService;
};

const httpStatus = require('http-status');
const { Message } = require('../../models');
const ApiError = require('../../utils/ApiError');

module.exports = (sequelize, Sequelize) => {
  class MessageService {

      /**
     * Create a Message
     * @param {Object} Message
     * @param {String} userId
     * @returns {Promise<Message>}
     */
    static createMessage = async (Message,userId) => {
      return new Promise(async (resolve, reject) => {
      await sequelize.query(
        'qdc_message_insert_data'
        + ' @cm_cal_id = :cm_cal_id,'
        + ' @cm_message = :cm_message,'
        
        + " @createdBy = '"+userId+"'"
        ,{
            replacements: Message
        },
        Sequelize.QueryTypes.INSERT
        )
        .then(result => {
          resolve(result);
        })
        .catch(error => {
          reject(error);
        });
      })
    };

    /**
     * Get Message
     * @param {Object} Message
     * @param {String} userId
     * @returns {Promise<Message>}
     */
    static getMessage = async (Message,userId) => {
      return new Promise(async(resolve, reject) => {
        await sequelize.query(
          "qdc_message_get_data"
              + ' @cm_cal_id = :cm_cal_id,'

              + " @searchBy = '"+userId+"'"
          ,{
              replacements: Message
          }
          ,Sequelize.QueryTypes.SELECT
          )
          .then(result => {
            resolve(result);
          })
          .catch(error => {
            reject(error);
          });
      })
    };


  }
  return MessageService;
};

const httpStatus = require('http-status');
const { Gauge } = require('../../models');
const ApiError = require('../../utils/ApiError');

module.exports = (sequelize, Sequelize) => {
  class CommonService {

    /**
     * Get Line
     */
    static searchLine = async () => {
      return new Promise(async(resolve, reject) => {
        await sequelize.query(
          "qdc_common_line_data"
          ,Sequelize.QueryTypes.SELECT
          )
          .then(result => {
            resolve(result);
          })
          .catch(error => {
            reject(error);
          });
      })
    };

     /**
     * Get Common System
     */
     static systemCommon = async (data, userId) => {
      return new Promise(async(resolve, reject) => {
        await sequelize.query(
          `SELECT * FROM TB_M_SYSTEM WHERE SYSTEM_TYPE_CD = '${data.SYSTEM_TYPE_CD}' 
             AND SYSTEM_SUB_TYPE = '${data.SYSTEM_SUB_TYPE}'`
          ,Sequelize.QueryTypes.SELECT
          )
          .then(result => {
            resolve(result);
          })
          .catch(error => {
            reject(error);
          });
      })
    };

     /**
     * Get Common Password
     */
     static passwordCommon = async (data, userId) => {
      return new Promise(async(resolve, reject) => {
        await sequelize.query(
          `SELECT 
              CASE 
                WHEN EXISTS (
                  SELECT 1 
                  FROM TB_M_SYSTEM 
                  WHERE SYSTEM_TYPE_CD = 'QDC' 
                    AND SYSTEM_SUB_TYPE = 'GENERATE_PLANNING' 
                    AND SYSTEM_CD = 'PASSWORD'
                    AND SYSTEM_VALUE_TXT = '${data.password}' 
                ) 
                THEN 'TRUE' 
                ELSE 'FALSE' 
              END AS STATUS
          `
          ,Sequelize.QueryTypes.SELECT
          )
          .then(result => {
            resolve(result);
          })
          .catch(error => {
            reject(error);
          });
      })
    };

  }
  return CommonService;
};

const httpStatus = require('http-status');
const { Gauge } = require('../../models');
const ApiError = require('../../utils/ApiError');

module.exports = (sequelize, Sequelize) => {
  class CalibrationItemService {

      /**
     * Create a Calibration Item
     * @param {Object} CalibrationItem
     * @param {String} userId
     * @returns {Promise<CalibrationItem>}
     */
    static createCalItem = async (CalibrationItem,userId) => {
      // return new Promise(async (resolve, reject) => {
      // await sequelize.query(
      //   'qdc_calibration_item_insert_data'
      //   + ' @GdGaugeId = :gd_gauge_id,'
      //   + ' @GdGaugeItem = :gd_gauge_item,'
      //   + ' @GdGaugeItemCal = :gd_gauge_item_cal,'
      //   + ' @GdGaugeItemCalMin = :gd_gauge_item_cal_min,'
      //   + ' @GdGaugeItemCalMax = :gd_gauge_item_cal_max,'
        
      //   + " @createdBy = '"+userId+"'"
      //   ,{
      //       replacements: CalibrationItem
      //   },
      //   Sequelize.QueryTypes.INSERT
      //   )
      //   .then(result => {
      //     resolve(result);
      //   })
      //   .catch(error => {
      //     reject(error);
      //   });
      // })
      return new Promise(async (resolve, reject) => {
        await sequelize.query(
          'qdc_calibration_item_insert_data'
          + ' @calItems = :cal_items,'
                
          + " @createdBy = '"+userId+"'"
          ,{
              replacements: CalibrationItem
          },
          Sequelize.QueryTypes.INSERT
          )
          .then(result => {
            resolve(result);
          })
          .catch(error => {
            reject(error);
          });
        })
    };


    /**
     * Update a Calibration Item
     * @param {Object} CalibrationItem
     * @param {String} userId
     * @returns {Promise<CalibrationItem>}
     */
     static updateCalItem = async (CalibrationItem,userId) => {
      return new Promise(async (resolve, reject) => {
      await sequelize.query(
        'qdc_calibration_item_update_data'
        + ' @GdId = :gd_id,'
        + ' @GdGaugeId = :gd_gauge_id,'
        + ' @GdGaugeItem = :gd_gauge_item,'
        + ' @GdGaugeItemCal = :gd_gauge_item_cal,'
        + ' @GdGaugeItemCalMin = :gd_gauge_item_cal_min,'
        + ' @GdGaugeItemCalMax = :gd_gauge_item_cal_max,'
        
        + " @changedBy = '"+userId+"'"
        ,{
            replacements: CalibrationItem
        },
        Sequelize.QueryTypes.UPDATE
        )
        .then(result => {
          resolve(result);
        })
        .catch(error => {
          reject(error);
        });
      })
    };

    static paginateCalItems = async (filter, options, userId) => {
      return new Promise(async(resolve, reject) => {
        let params = JSON.parse(JSON.stringify(filter));
        params.page = options.page;
        params.limit = options.limit;
        params.sortBy = options.sortBy;
        params.sortOrder = options.sortOrder;

        await sequelize.query(
            "qdc_calibration_item_search_data"
                      + " @keyword = :keyword,"
                      + " @idGauge = :idGauge,"
                      + " @searchBy = '" + userId + "',"
                      + " @page = :page,"
                      + " @rowPerPage = :limit,"
                      + " @orderBy = :sortBy,"
                      + " @sortOrder = :sortOrder"
          ,{
              replacements: params
          }
          ,Sequelize.QueryTypes.SELECT
          )
          .then(result => {
            if(result.length == 0){
              resolve({count:0, rows:[]});
            }else
            if(result[0][0]['FNAME'] && result[0][0]['FNAME']=='ROW_COUNT'){
              let rowCount = result[0].splice(0,1);
              let count = rowCount[0]['FVAL'];
              let rows = result[0];
              resolve({count:count, rows: rows});
            }else{
              resolve(result);
            }
          })
          .catch(error => {
            reject(error);
          });
      })
    };

    /**
     * Query for 
     * @param {Object} filter - Sequelize filter
     * @param {Object} options - Query options
     * @param {string} [options.sortOrder] - Sort option in the format: ['field1'],['field2']
     * @param {string} [options.sortBy] - Sort option in the format: ['ASC'],['DESC']
     * @param {number} [options.limit] - Maximum number of results per page (default = 10)
     * @param {number} [options.page] - Current page (default = 1)
     * @param {String} userId
     * @returns {Promise<QueryResult>}
     */
    static queryCalItems = async (filter, options, userId) => {
      const Data = await this.paginateCalItems(filter, options, userId);
      return Data;
    };

    /**
     * Delete Calibration Item
     * @param {Object} CalibrationItem
     * @param {String} userId
     * @returns {Promise<Gauge>}
     */
    static deleteCalItem = async (CalibrationItem,userId) => {
      await sequelize.query(
        'qdc_calibration_item_delete_data'
        + ' @GdId = :gd_id,'

        + " @changedBy = '"+userId+"'"
        ,{
            replacements: CalibrationItem
        },
        Sequelize.QueryTypes.UPDATE
        )
        .then(result => {
          return result;
        })
        .catch(error => {
          throw(error);
        });
    };

    /**
     * Get Calibration Item
     * @param {Object} CalibrationItem
     * @param {String} userId
     * @returns {Promise<Gauge>}
     */
    static getCalItem = async (CalibrationItem,userId) => {
      return new Promise(async(resolve, reject) => {
        await sequelize.query(
          "qdc_calibration_item_get_data"
              + ' @GdId = :gd_id,'

              + " @searchBy = '"+userId+"'"
          ,{
              replacements: CalibrationItem
          }
          ,Sequelize.QueryTypes.SELECT
          )
          .then(result => {
            resolve(result);
          })
          .catch(error => {
            reject(error);
          });
      })
    };


  }
  return CalibrationItemService;
};

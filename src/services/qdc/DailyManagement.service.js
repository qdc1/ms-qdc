const httpStatus = require('http-status');
const { Gauge } = require('../../models');
const ApiError = require('../../utils/ApiError');

module.exports = (sequelize, Sequelize) => {
  class DailyManagementService {

    /**
     * Get DailyManagement
     * @param {Object} DailyManagement
     * @param {String} userId
     * @returns {Promise<DailyManagement>}
     */
    static getDailyManagement = async (DailyManagement,userId) => {
        return new Promise(async(resolve, reject) => {
          await sequelize.query(
            "qdc_daily_management_get_data"
                + ' @month = :month,'
                + ' @year = :year,'
  
                + " @searchBy = '"+userId+"'"
            ,{
                replacements: DailyManagement
            }
            ,Sequelize.QueryTypes.SELECT
            )
            .then(result => {
              resolve(result);
            })
            .catch(error => {
              reject(error);
            });
        })
      };

        /**
     * Create a DailyManagement
     * @param {Object} DailyManagement
     * @param {String} userId
     * @returns {Promise<DailyManagement>}
     */
    static createDailyManagement = async (DailyManagement,userId) => {
      return new Promise(async (resolve, reject) => {
        await sequelize.query(
          'qdc_daily_management_insert_data'
          + ' @month = :month,'
          + ' @year = :year,'
          + ' @jsonData = :dataReport,'
          
          + " @createdBy = '"+userId+"'"
          ,{
              replacements: DailyManagement
          },
          Sequelize.QueryTypes.INSERT
          )
          .then(result => {
            resolve(result);
          })
          .catch(error => {
            reject(error);
          });
        })
    };

    /**
     * Update a DailyManagement
     * @param {Object} DailyManagement
     * @param {String} userId
     * @returns {Promise<DailyManagement>}
     */
    static updateDailyManagement = async (DailyManagement,userId) => {
      console.log(DailyManagement)
      return new Promise(async (resolve, reject) => {
      await sequelize.query(
        'qdc_daily_management_update_data'
        + ' @jsonData = :dataReport,'
        
        + " @changedBy = '"+userId+"'"
        ,{
            replacements: DailyManagement
        },
        Sequelize.QueryTypes.UPDATE
        )
        .then(result => {
          resolve(result);
        })
        .catch(error => {
          reject(error);
        });
      })
    };


  }
  return DailyManagementService;
};

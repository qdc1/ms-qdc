const httpStatus = require('http-status');
const { Gauge } = require('../../models');
const ApiError = require('../../utils/ApiError');

module.exports = (sequelize, Sequelize) => {
  class CustomPlanningService {

      /**
     * Create a Custom Planning
     * @param {Object} CustomPlanningItem
     * @param {String} userId
     * @returns {Promise<CustomPlanningItem>}
     */
    static createCustomPlanning = async (CustomPlanningItem,userId) => {
      return new Promise(async (resolve, reject) => {
        await sequelize.query(
          'qdc_custom_monthly_planning_insert_data'
          + ' @gpdDate = :gpd_date,'
          + ' @gpdGaugeId = :gpd_gauge_id,'
                
          + " @createdBy = '"+userId+"'"
          ,{
              replacements: CustomPlanningItem
          },
          Sequelize.QueryTypes.INSERT
          )
          .then(result => {
            resolve(result);
          })
          .catch(error => {
            reject(error);
          });
        })
    };


    /**
     * Update a Custom Planning
     * @param {Object} CustomPlanningItem
     * @param {String} userId
     * @returns {Promise<CustomPlanningItem>}
     */
     static updateCustomPlanning = async (CustomPlanningItem,userId) => {
      return new Promise(async (resolve, reject) => {
      await sequelize.query(
        'qdc_custom_monthly_planning_update_data'
        + ' @gpdId = :gpd_id,'
        + ' @gpdDate = :gpd_date,'
        
        + " @changedBy = '"+userId+"'"
        ,{
            replacements: CustomPlanningItem
        },
        Sequelize.QueryTypes.UPDATE
        )
        .then(result => {
          resolve(result);
        })
        .catch(error => {
          reject(error);
        });
      })
    };

    static paginateCustomPlanning = async (filter, options, userId) => {
      return new Promise(async(resolve, reject) => {
        let params = JSON.parse(JSON.stringify(filter));
        params.page = options.page;
        params.limit = options.limit;
        params.sortBy = options.sortBy;
        params.sortOrder = options.sortOrder;
        console.log(params)
        await sequelize.query(
            "qdc_custom_monthly_planning_search_data"
                      + " @keyword = :keyword,"
                      + " @searchBy = '" + userId + "',"
                      + " @startDate = :startDate,"
                      + " @endDate = :endDate,"
                      + " @page = :page,"
                      + " @rowPerPage = :limit,"
                      + " @orderBy = :sortBy,"
                      + " @sortOrder = :sortOrder"
          ,{
              replacements: params
          }
          ,Sequelize.QueryTypes.SELECT
          )
          .then(result => {
            if(result.length == 0){
              resolve({count:0, rows:[]});
            }else
            if(result[0][0]['FNAME'] && result[0][0]['FNAME']=='ROW_COUNT'){
              let rowCount = result[0].splice(0,1);
              let count = rowCount[0]['FVAL'];
              let rows = result[0];
              resolve({count:count, rows: rows});
            }else{
              resolve(result);
            }
          })
          .catch(error => {
            reject(error);
          });
      })
    };

    /**
     * Query for 
     * @param {Object} filter - Sequelize filter
     * @param {Object} options - Query options
     * @param {string} [options.sortOrder] - Sort option in the format: ['field1'],['field2']
     * @param {string} [options.sortBy] - Sort option in the format: ['ASC'],['DESC']
     * @param {number} [options.limit] - Maximum number of results per page (default = 10)
     * @param {number} [options.page] - Current page (default = 1)
     * @param {String} userId
     * @returns {Promise<QueryResult>}
     */
    static queryCustomPlanning = async (filter, options, userId) => {
      const Data = await this.paginateCustomPlanning(filter, options, userId);
      return Data;
    };

    /**
     * Delete Custom Planning
     * @param {Object} CustomPlanningItem
     * @param {String} userId
     * @returns {Promise<Gauge>}
     */
    static deleteCustomPlanning = async (CustomPlanningItem,userId) => {
      await sequelize.query(
        'qdc_custom_monthly_planning_delete_data'
        + ' @GpdId = :gpd_id,'

        + " @changedBy = '"+userId+"'"
        ,{
            replacements: CustomPlanningItem
        },
        Sequelize.QueryTypes.UPDATE
        )
        .then(result => {
          return result;
        })
        .catch(error => {
          throw(error);
        });
    };


  }
  return CustomPlanningService;
};

const httpStatus = require('http-status');
const { Gauge } = require('../../models');
const ApiError = require('../../utils/ApiError');

module.exports = (sequelize, Sequelize) => {
  class GaugeDetail {

    /**
     * Get Gauge
     * @param {Object} GaugeDetail
     * @param {String} userId
     * @returns {Promise<Gauge>}
     */
    static getGaugeDetail = async (GaugeDetail,userId) => {
        return new Promise(async(resolve, reject) => {
          await sequelize.query(
            "qdc_gauge_detail_get_data"
                + ' @id = :id,'
                + ' @columnBy = :columnBy,'
  
                + " @searchBy = '"+userId+"'"
            ,{
                replacements: GaugeDetail
            }
            ,Sequelize.QueryTypes.SELECT
            )
            .then(result => {
              resolve(result);
            })
            .catch(error => {
              reject(error);
            });
        })
      };

      static getChartDataDetail = async (GaugeDetail, userId) =>  {
        return new Promise(async(resolve, reject) => {
          await sequelize.query(
            "qdc_gauge_detail_get_chart_data"
                + ' @id = :id,'
                
                + " @searchBy = '"+userId+"'"
                ,{
              replacements: GaugeDetail
          },
            Sequelize.QueryTypes.SELECT
          )
          .then(result => {
            resolve(result);
          })
          .catch(error => {
            reject(error);
          });
        })
      };

  }
  return GaugeDetail;
};

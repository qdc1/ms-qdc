const express = require('express');
const docsRoute = require('./docs.route');
const config = require('../config/config');

const GaugeRoute = require('./qdc/Guage.route');
const GaugeDetailRoute = require('./qdc/GaugeDetail.route');
const QualityOperationalRoute = require('./qdc/QualityOperational.route');
const CalibrationRoute = require('./qdc/Calibration.route');
const CalibrationItemRoute = require('./qdc/CalibrationItem.route');
const MonthlyPlanningRoute = require('./qdc/MonthlyPlanning.route');
const MonthlyPlanningHolidayRoute = require('./qdc/MonthlyPlanningHoliday.route');
const DailyManagementRoute = require('./qdc/DailyManagement.route');
const CustomPlanningRoute = require('./qdc/CustomPlanning.route');
const ReportPlanningRoute = require('./qdc/ReportPlanning.route');
const MessageRoute = require('./qdc/Message.route');
const CommonRoute = require('./qdc/Common.route');
/*define const other in here*/

const router = express.Router();

const defaultRoutes = [
  {
    path: '/gauge',
    route: GaugeRoute,
  },
  {
    path: '/gauge-detail',
    route: GaugeDetailRoute,
  },
  {
    path: '/quality-operational',
    route: QualityOperationalRoute
  },
  {
    path: '/calibration',
    route: CalibrationRoute
  },
  {
    path: '/calibration-item',
    route: CalibrationItemRoute
  },
  {
    path: '/monthly-planning',
    route: MonthlyPlanningRoute
  },
  {
    path: '/monthly-planning-holiday',
    route: MonthlyPlanningHolidayRoute
  },
  {
    path: '/daily-management',
    route: DailyManagementRoute
  },
  {
    path: '/custom-planning',
    route: CustomPlanningRoute
  },
  {
    path: '/report-planning',
    route: ReportPlanningRoute
  },
  {
    path: '/message',
    route: MessageRoute
  },
  {
    path: '/common',
    route: CommonRoute,
  }
/*define other in here*/
];

const devRoutes = [
  // routes available only in development mode
  {
    path: '/docs',
    route: docsRoute,
  },
];

defaultRoutes.forEach((route) => {
  router.use(route.path, route.route);
});

/* istanbul ignore next */
if (config.env === 'dev' || config.env === 'local' ) {
  devRoutes.forEach((route) => {
    router.use(route.path, route.route);
  });
}

module.exports = router;

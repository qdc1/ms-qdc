const express = require('express');
const auth = require('../../middlewares/auth');
const validate = require('../../middlewares/validate');
const messageValidation = require('../../validations/qdc/Message.validation');
const messageController = require('../../controllers/qdc/Message.controller');

const router = express.Router();

router.route('/add').post(auth(), validate(messageValidation.createMessage), messageController.createMessage);
router.route('/get').post(auth(), validate(messageValidation.getMessage), messageController.getMessage);

module.exports = router;

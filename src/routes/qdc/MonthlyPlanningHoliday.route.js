const express = require('express');
const auth = require('../../middlewares/auth');
const validate = require('../../middlewares/validate');
const monthlyPlanningHolidayValidation = require('../../validations/qdc/MonthlyPlanningHoliday.validation');
const MonthlyPlanningHolidayController = require('../../controllers/qdc/MonthlyPlanningHoliday.controller');

const router = express.Router();


router.route('/add').post(auth(), validate(monthlyPlanningHolidayValidation.createMonthlyPlanningHoliday), MonthlyPlanningHolidayController.createMonthlyPlanningHoliday);
router.route('/edit').post(auth(), validate(monthlyPlanningHolidayValidation.updateMonthlyPlanningHoliday), MonthlyPlanningHolidayController.updateMonthlyPlanningHoliday);
router.route('/delete').post(auth(), validate(monthlyPlanningHolidayValidation.deleteMonthlyPlanningHoliday), MonthlyPlanningHolidayController.deleteMonthlyPlanningHoliday);
router.route('/search').post(auth(), validate(monthlyPlanningHolidayValidation.searchMonthlyPlanningHoliday), MonthlyPlanningHolidayController.searchMonthlyPlanningHoliday);
router.route('/get').post(auth(), validate(monthlyPlanningHolidayValidation.getMonthlyPlanningHoliday), MonthlyPlanningHolidayController.getMonthlyPlanningHoliday);


module.exports = router;

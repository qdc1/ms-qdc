const express = require('express');
const auth = require('../../middlewares/auth');
const validate = require('../../middlewares/validate');
const qtyOperationalValidation = require('../../validations/qdc/QualityOperational.validation');
const qtyOperationalCpntroller = require('../../controllers/qdc/QualityOperational.controller');

const router = express.Router();

// router.route('/add').post(auth(), validate(qtyOperationalValidation.createDie), qtyOperationalCpntroller.createDie);
// router.route('/edit').post(auth(), validate(qtyOperationalValidation.updateDie), qtyOperationalCpntroller.updateDie);
// router.route('/delete').post(auth(), validate(qtyOperationalValidation.deleteGauge), qtyOperationalCpntroller.deleteGauge);
router.route('/search').post(auth(), qtyOperationalCpntroller.searchQtyOperational);
// router.route('/view').post(auth(), validate(qtyOperationalValidation.getGauge), qtyOperationalCpntroller.getGauge);

module.exports = router;

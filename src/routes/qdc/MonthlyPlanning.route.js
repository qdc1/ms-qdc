const express = require('express');
const auth = require('../../middlewares/auth');
const validate = require('../../middlewares/validate');
const monthlyPlanningValidation = require('../../validations/qdc/MonthlyPlanning.validation');
const MonthlyPlanningController = require('../../controllers/qdc/MonthlyPlanning.controller');

const router = express.Router();


router.route('/add').post(auth(), validate(monthlyPlanningValidation.createMonthlyPlanning), MonthlyPlanningController.createMonthlyPlanning);
router.route('/search').post(auth(), validate(monthlyPlanningValidation.searchMonthlyPlanning), MonthlyPlanningController.searchMonthlyPlanning);
router.route('/get').post(auth(), validate(monthlyPlanningValidation.getMonthlyPlanning), MonthlyPlanningController.getMonthlyPlanning);
router.route('/download').post(auth(), MonthlyPlanningController.getMonthlyPlanningLineReport);


module.exports = router;

const express = require('express');
const auth = require('../../middlewares/auth');
const validate = require('../../middlewares/validate');
const upload = require("../../middlewares/upload");
const reportPlanningValidation = require('../../validations/qdc/ReportPlanning.validation');
const customPlanningController = require('../../controllers/qdc/ReportPlanning.controller');

const router = express.Router();

router.route('/get').post(auth(), validate(reportPlanningValidation.getReportPlanning), customPlanningController.getReportPlanning);

module.exports = router;

const express = require('express');
const auth = require('../../middlewares/auth');
const validate = require('../../middlewares/validate');
const commonController = require('../../controllers/qdc/Common.controller');
const commonValidation = require('../../validations/qdc/Common.validation');

const router = express.Router();

router.route('/line/search').post(commonController.searchLine);
router.route('/system/search').post(commonController.systemCommon);
router.route('/password/search').post(validate(commonValidation.passwordCommon), commonController.passwordCommon);

module.exports = router;

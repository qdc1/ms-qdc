const express = require('express');
const auth = require('../../middlewares/auth');
const validate = require('../../middlewares/validate');
const dailyManagementValidation = require('../../validations/qdc/DailyManagement.validation');
const dailyManagementController = require('../../controllers/qdc/DailyManagement.controller');

const router = express.Router();


router.route('/add').post(auth(), validate(dailyManagementValidation.createDailyManagement), dailyManagementController.createDailyManagement);
router.route('/edit').post(auth(), validate(dailyManagementValidation.updateDailyManagement), dailyManagementController.updateDailyManagement);
// router.route('/delete').post(auth(), validate(dailyManagementValidation.deleteGauge), dailyManagementController.deleteGauge);
// router.route('/search').post(auth(), validate(dailyManagementValidation.searchGauge), dailyManagementController.searchGauge);
router.route('/get').post(auth(), validate(dailyManagementValidation.getDailyManagement), dailyManagementController.getDailyManagement);

module.exports = router;

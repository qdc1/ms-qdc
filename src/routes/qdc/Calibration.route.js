const express = require('express');
const auth = require('../../middlewares/auth');
const validate = require('../../middlewares/validate');
const calibrationValidation = require('../../validations/qdc/Calibration.validation');
const calibrationController = require('../../controllers/qdc/Calibration.controller');

const router = express.Router();


router.route('/add').post(auth(), validate(calibrationValidation.createCalibration), calibrationController.createCalibration);
router.route('/edit').post(auth(), validate(calibrationValidation.updateCalibration), calibrationController.updateCalibration);
router.route('/delete').post(auth(), validate(calibrationValidation.deleteCalibration), calibrationController.deleteCalibration);
router.route('/search').post(auth(), validate(calibrationValidation.searchCalibration), calibrationController.searchCalibration);
router.route('/get').post(auth(), validate(calibrationValidation.getCalibrationValue), calibrationController.getCalibrationValue);
router.route('/edit-status').post(auth(), validate(calibrationValidation.updateCalibrationStatus), calibrationController.updateCalibrationStatus);
router.route('/draft').post(auth(), validate(calibrationValidation.createCalibrationDraft), calibrationController.createCalibrationDraft);
router.route('/get-draft').post(auth(), validate(calibrationValidation.getCalibrationDraft), calibrationController.getCalibrationDraft);

module.exports = router;

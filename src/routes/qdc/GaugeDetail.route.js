const express = require('express');
const auth = require('../../middlewares/auth');
const validate = require('../../middlewares/validate');
const gaugeDetailValidation = require('../../validations/qdc/GaugeDetail.validation');
const gaugeDetailController = require('../../controllers/qdc/GaugeDetail.controller');

const router = express.Router();


// router.route('/add').post(auth(), validate(gaugeDetailValidation.createGauge), gaugeDetailController.createGauge);
// router.route('/edit').post(auth(), validate(gaugeDetailValidation.updateGauge), gaugeDetailController.updateGauge);
// router.route('/delete').post(auth(), validate(gaugeDetailValidation.deleteGauge), gaugeDetailController.deleteGauge);
// router.route('/search').post(auth(), validate(gaugeDetailValidation.searchGauge), gaugeDetailController.searchGauge);
router.route('/get').post(auth(), validate(gaugeDetailValidation.getGaugeDetail), gaugeDetailController.getGaugeDetail);
router.route('/download').post(auth(), gaugeDetailController.downloadGaugeDetail);

module.exports = router;

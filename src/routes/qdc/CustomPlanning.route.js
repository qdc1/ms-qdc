const express = require('express');
const auth = require('../../middlewares/auth');
const validate = require('../../middlewares/validate');
const upload = require("../../middlewares/upload");
const customPlanningValidation = require('../../validations/qdc/CustomPlanning.validation');
const customPlanningController = require('../../controllers/qdc/CustomPlanning.controller');

const router = express.Router();


router.route('/add').post(auth(), validate(customPlanningValidation.createCustomPlanning), customPlanningController.createCustomPlanning);
router.route('/edit').post(auth(), validate(customPlanningValidation.updateCustomPlanning), customPlanningController.updateCustomPlanning);
router.route('/delete').post(auth(), validate(customPlanningValidation.deleteCustomPlanning), customPlanningController.deleteCustomPlanning);
router.route('/search').post(auth(), validate(customPlanningValidation.searchCustomPlanning), customPlanningController.searchCustomPlanning);

module.exports = router;

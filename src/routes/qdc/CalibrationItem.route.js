const express = require('express');
const auth = require('../../middlewares/auth');
const validate = require('../../middlewares/validate');
const calItemValidation = require('../../validations/qdc/CalibrationItem.validation');
const calItemController = require('../../controllers/qdc/CalibrationItem.controller');

const router = express.Router();


router.route('/add').post(auth(), validate(calItemValidation.createCalItem), calItemController.createCalItem);
router.route('/edit').post(auth(), validate(calItemValidation.updateCalItem), calItemController.updateCalItem);
router.route('/delete').post(auth(), validate(calItemValidation.deleteCalItem), calItemController.deleteCalItem);
router.route('/search').post(auth(), validate(calItemValidation.searchCalItem), calItemController.searchCalItem);
router.route('/get').post(auth(), validate(calItemValidation.getCalItem), calItemController.getCalItem);

module.exports = router;
